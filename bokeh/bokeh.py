import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn import manifold

import os
import pickle
import sys
import sqlite3
import math
import datetime

from bokeh.palettes import Category20 as palette
import itertools
import bokeh.models as bmo
from bokeh.palettes import d3

from sklearn.metrics import confusion_matrix
import numpy as np
from bokeh.models.widgets import Div, Slider, Dropdown, Paragraph, TableColumn, DateFormatter, DataTable, DateSlider, CheckboxButtonGroup, RadioButtonGroup
from bokeh.io import curdoc
from bokeh.layouts import widgetbox
from bokeh.layouts import row, column
from bokeh.plotting import figure
from bokeh.plotting import show, ColumnDataSource
from bokeh.models import ColumnDataSource, Label, HoverTool
from bokeh.models import LabelSet
from bokeh.models.annotations import Title

from bokeh.tile_providers import CARTODBPOSITRON
from bokeh.tile_providers import STAMEN_TONER

### GLOBAL VARIABLES ###
WORKING_DIR = os.path.dirname(os.path.abspath(__file__))
PICKLE_DF = 'bokeh_df.pkl'
START_DATE = "01-10-2018"
START_TEST_DATE = "01-11-2018"

### PICKLE FACTORY ###

def save_pickle(result, pickle_name):
    """Saves an object in pickle object file for future use"""
    with open(os.path.join(WORKING_DIR, pickle_name), 'wb') as file:
        pickle.dump(result, file)
        return True

def load_pickle(pickle_name):
    """Loads a previously stored model dictionary with pickle"""
    with open(os.path.join(WORKING_DIR, pickle_name), 'rb') as handle:
        return pickle.load(handle)


if os.path.exists(os.path.join(WORKING_DIR, PICKLE_DF)):
    df = load_pickle(PICKLE_DF)
else:
    print('Missing df pickle : run bokeh_modeling.py !')


def merc(Coords):
    lat = Coords[0]
    lon = Coords[1]

    r_major = 6378137.000
    x = r_major * math.radians(lon)
    scale = x / lon
    y = 180.0 / math.pi * math.log(math.tan(math.pi / 4.0 +
                                            lat * (math.pi / 180.0) / 2.0)) * scale
    return (x, y)


df['coords_x'] = df.apply(lambda x: merc((x.Latitude, x.Longitude))[0], axis=1)
df['coords_y'] = df.apply(lambda x: merc((x.Latitude, x.Longitude))[1], axis=1)

df = df[df['Date'] >= datetime.datetime.strptime(START_DATE, "%d-%m-%Y")]


## DATA SOURCE ##

source = ColumnDataSource(df)
source_predict = ColumnDataSource(df)
source_both = ColumnDataSource(pd.DataFrame(columns=df.columns))

## SLIDERS ##

start_date = DateSlider(title='Start_date',
                        value=datetime.datetime.strptime(START_TEST_DATE, "%d-%m-%Y"),
                        start=min(df['Date']),
                        end=max(df['Date']),
                        step=1)

end_date = DateSlider(title='End_date',
                        value=max(df['Date']),
                        start=min(df['Date']),
                        end=max(df['Date']),
                        step=1)

start_hour = Slider(title='Start_hour',
                        value=min(df['Hour']),
                        start=min(df['Hour']),
                        end=max(df['Hour']),
                        step=1)

end_hour = Slider(title='End_hour',
                        value=max(df['Hour']),
                        start=min(df['Hour']),
                        end=max(df['Hour']),
                        step=1)

## TEXT ##
output_analytics = Div()

categories = list(pd.unique(df['PrimaryType'].values))
type_of_crime = CheckboxButtonGroup(labels=categories, active=[0])
prediction_checkbox = RadioButtonGroup(labels=['All', 'Display correct'], active=0)

p1 = figure(plot_width=500, plot_height=500, x_axis_type="mercator", y_axis_type="mercator", title=Title(text='True Data'))
p1.add_tile(STAMEN_TONER)

p2 = figure(plot_width=500, plot_height=500, x_axis_type="mercator", y_axis_type="mercator", title=Title(text='Predictions'))
p2.add_tile(STAMEN_TONER)

# create a color iterator
colors = itertools.cycle(palette[20])

"""
for color, category in zip(colors, pd.unique(df['PrimaryType'].values)):
    source_ = ColumnDataSource(df[df['PrimaryType'] == category])
    p.circle(x='coords_x', y='coords_y', source=source_, legend=category, color=color)
"""
"""palette = d3['Category10'][len(df['cat'].unique())]
color_map = bmo.CategoricalColorMapper(factors=df['cat'].unique(),
                                   palette=palette)
"""
color_map = bmo.CategoricalColorMapper(factors=pd.unique(df['PrimaryType'].values), palette=palette[10])
p1.scatter(source=source,
          x='coords_x',
          y='coords_y',
          #radius = 's',
          legend='PrimaryType',
          color={'field': 'PrimaryType', 'transform': color_map},
          marker="circle",
          alpha=0.75)
          #fill_color=colors)
          #line_color=colors)
p1.scatter(source=source_both,
          x='coords_x',
          y='coords_y',
          #radius = 's',
          legend=False,
          line_color='green', fill_color="green",
          marker="circle",
          alpha=0.50)
          #fill_color=colors)
          #line_color=colors)

p2.scatter(source=source_predict,
          x='coords_x',
          y='coords_y',
          #radius = 's',
          legend='Prediction',
          color={'field': 'Prediction', 'transform': color_map},
          marker="circle",
          alpha=0.75)
          #fill_color=colors)
          #line_color=colors)

p2.scatter(source=source_both,
          x='coords_x',
          y='coords_y',
          #radius = 's',
          legend=False,
           line_color='green', fill_color="green",
          marker="circle",
          alpha=0.50)
          #fill_color=colors)
          #line_color=colors)

p2.legend.click_policy="hide"
p1.legend.click_policy="hide"

## DEFINE CALLBACK UPDATE ##

def update_data(attrname, old, new):
    start_date_ = start_date.value
    end_date_ = end_date.value
    start_hour_ = start_hour.value
    end_hour_ = end_hour.value
    category_index = type_of_crime.active
    labeled_only = prediction_checkbox.active

    df_ = df[df['Date'] >= start_date_][df['Date'] <= end_date_][df['Hour'] >= start_hour_][df['Hour'] <= end_hour_]
    source.data = source.from_df(
        df_[df_['PrimaryType'].isin([categories[i] for i in category_index])]
    )
    source_predict.data = source_predict.from_df(
        df_[df_['Prediction'].isin([categories[i] for i in category_index])]
    )

    if labeled_only == 0:
        #empty data source
        output_analytics.text = ""
        source_both.data = source_predict.from_df(pd.DataFrame(columns=df_.columns))
    else:
        df__ = df_[df_['Prediction'].isin([categories[i] for i in category_index])][df_['PrimaryType'] == df_['Prediction']]
        source_both.data = source_both.from_df(df__)
        output_analytics.text = ""
        for category in [categories[i] for i in category_index]:
            recall = round(len(df__[df__['PrimaryType'] == category])/len(df_[df_['PrimaryType'] == category]), 2)
            precision = round(len(df__[df__['Prediction'] == category])/len(df_[df_['Prediction'] == category]), 2)
            f1 = round(2 * precision * recall / (precision + recall), 2)
            output_analytics.text += '</br>' + str(category)
            output_analytics.text += '</br>' + 'Recall    : {}'.format(recall)
            output_analytics.text += '</br>' + 'Precision : {}'.format(precision)
            output_analytics.text += '</br>' + 'F1        : {}'.format(f1)
            output_analytics.text += '</br>'

update_data(None, None, None)

start_date.on_change('value', update_data)
end_date.on_change('value', update_data)
start_hour.on_change('value', update_data)
end_hour.on_change('value', update_data)
type_of_crime.on_change('active', update_data)
prediction_checkbox.on_change('active', update_data)

curdoc().add_root(column(row(start_date, start_hour), row(end_date, end_hour), row(column(type_of_crime, prediction_checkbox, output_analytics), p1, p2)))
curdoc().title = "Chicago Crime !"