import os
import pickle
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import collections
import sqlite3

from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn import metrics
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report

WORKING_DIR = os.path.dirname(os.path.abspath(__file__))
MAP_CATEGORIES = {
    'THEFT' : 'THEFT / ROBBERY',
    'BATTERY' : 'PHYSICAL VIOLENCE',
    'CRIMINAL DAMAGE' : 'CRIMINAL DAMAGE',
    'ASSAULT' : 'PHYSICAL VIOLENCE',
    'DECEPTIVE PRACTICE' : 'STOLEN PROPERTY',
    'OTHER OFFENSE' : 'OTHER OFFENSE',
    'NARCOTICS' : 'DRUG / NARCOTIC / ALCOHOL',
    'BURGLARY' : 'BURGLARY',
    'ROBBERY' : 'THEFT / ROBBERY',
    'MOTOR VEHICLE THEFT' : 'MOTOR VEHICLE THEFT',
    'CRIMINAL TRESPASS' : 'BURGLARY',
    'WEAPONS VIOLATION' : 'WEAPONS VIOLATION',
    'OFFENSE INVOLVING CHILDREN' : 'OTHER OFFENSE',
    'PUBLIC PEACE VIOLATION' : 'OTHER OFFENSE',
    'CRIM SEXUAL ASSAULT' : 'PROSTITUTION / SEX OFFENSE',
    'INTERFERENCE WITH PUBLIC OFFICER' : 'OTHER OFFENSE',
    'SEX OFFENSE' : 'PROSTITUTION / SEX OFFENSE',
    'PROSTITUTION' : 'PROSTITUTION / SEX OFFENSE',
    'HOMICIDE' : 'OTHER OFFENSE',
    'ARSON' : 'CRIMINAL DAMAGE',
    'LIQUOR LAW VIOLATION' : 'DRUG / NARCOTIC / ALCOHOL',
    'GAMBLING' : 'OTHER OFFENSE',
    'KIDNAPPING' : 'OTHER OFFENSE',
    'STALKING'  :'OTHER OFFENSE',
    'INTIMIDATION' : 'OTHER OFFENSE',
    'CONCEALED CARRY LICENSE VIOLATION' : 'OTHER OFFENSE',
    'OBSCENITY' : 'OTHER OFFENSE',
    'NON-CRIMINAL' : 'OTHER OFFENSE',
    'PUBLIC INDECENCY' : 'OTHER OFFENSE',
    'HUMAN TRAFFICKING' : 'OTHER OFFENSE',
    'NON - CRIMINAL' : 'OTHER OFFENSE',
    'OTHER NARCOTIC VIOLATION' : 'DRUG / NARCOTIC / ALCOHOL',
    'NON-CRIMINAL (SUBJECT SPECIFIED)' : 'OTHER OFFENSE'
}
DB_FILENAME = 'ChicagoCrime.sqlite'
PICKLE_MODEL = 'bokeh_model.pkl'
PICKLE_DATAFRAME = 'bokeh_df.pkl'
START_DATE = '2017-01-01'
TRAIN_TEST_YEAR = 2018
TRAIN_TEST_MONTH = 7

def format_dataframe(df):
    df.PrimaryType = df.PrimaryType.map(MAP_CATEGORIES)
    df = df[df.Latitude != ''][df.Longitude != '']
    df = df.rename(columns={"Beat": "Beat_num"})
    df.XCoordinate = df.XCoordinate.astype("float")
    df.YCoordinate = df.YCoordinate.astype("float")
    df.Latitude = df.Latitude.astype("float")
    df.Longitude = df.Longitude.astype("float")
    # Date
    df.loc[:, 'Date'] = pd.to_datetime(df.loc[:, 'Date'])
    df['Month'] = df.loc[:, 'Date'].apply(lambda x: x.month)
    df['Week'] = df.loc[:, 'Date'].apply(lambda x: x.week)
    df['Day'] = df.loc[:, 'Date'].apply(lambda x: x.weekday())
    df['Hour'] = df.loc[:, 'Date'].apply(lambda x: x.hour)
    df['Minute'] = df.loc[:, 'Date'].apply(lambda x: x.minute)
    return df

def drop_dataframe(df):
    final_feature = df.copy()
    final_feature = final_feature.drop(['Date', 'ID', 'CaseNumber',
                                        'NormalizedDate', 'Block', 'IUCR',
                                        'Description', 'Arrest', 'Domestic',
                                        'FBICode', 'UpdatedOn', 'Latitude', 'Longitude', 'Minute',
                                        'LocationDescription'], axis=1)
    return final_feature

def get_categorical(final_feature):
    c = final_feature.PrimaryType.astype('category')
    d = dict(enumerate(c.cat.categories))
    for i in final_feature.columns:
        if final_feature[i].dtype == "object":
            # transform all the non-categorical data to categorical
            final_feature[i] = pd.Categorical(final_feature[i]).codes
    return final_feature, d

def build_model(final_feature):
    final_feature_ = final_feature.copy()
    y = final_feature_.pop('PrimaryType')
    X = final_feature_
    #X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
    X_train = X[(X['Year'] < TRAIN_TEST_YEAR) | ((X['Year'] == TRAIN_TEST_YEAR) & (X['Month'] < TRAIN_TEST_MONTH))]
    y_train = y[(X['Year'] < TRAIN_TEST_YEAR) | ((X['Year'] == TRAIN_TEST_YEAR) & (X['Month'] < TRAIN_TEST_MONTH))]
    X_test = X[(X['Year'] == TRAIN_TEST_YEAR) & (X['Month'] >= TRAIN_TEST_MONTH)]
    y_test = y[(X['Year'] == TRAIN_TEST_YEAR) & (X['Month'] >= TRAIN_TEST_MONTH)]

    model = RandomForestClassifier(n_estimators=10, min_samples_leaf=10, min_samples_split=50, class_weight='balanced')
    model.fit(X_train, y_train)
    return model


def save_pickle(result, pickle_name):
    """Saves an object in pickle object file for future use"""
    with open(os.path.join(WORKING_DIR, pickle_name), 'wb') as file:
        pickle.dump(result, file)
        return True

def load_pickle(pickle_name):
    """Loads a previously stored model dictionary with pickle"""
    with open(os.path.join(WORKING_DIR, pickle_name), 'rb') as handle:
        return pickle.load(handle)

## LOAD FROM DATABASE ##

print('LOAD FROM DATABASE...')

with sqlite3.connect(DB_FILENAME) as con:
    cur = con.cursor()
    print("SELECT * FROM chicagoCrime WHERE Date>=date('{}');".format(START_DATE))
    result = cur.execute("SELECT * FROM chicagoCrime WHERE Date>=date('{}');".format(START_DATE))
    columns = [description[0] for description in cur.description]
    result = result.fetchall()
    cur.close()

## BUILD AND APPLY MODEL ##
print('BUILD AND APPLY MODEL...')

if os.path.exists(os.path.join(WORKING_DIR, PICKLE_MODEL)):
    clf = load_pickle(PICKLE_MODEL)
    df = pd.DataFrame(data=result, columns=columns)
    df_ = format_dataframe(df)
    final_feature = drop_dataframe(df_)
    final_feature, d = get_categorical(final_feature)
else:
    df = pd.DataFrame(data=result, columns=columns)
    df_ = format_dataframe(df)
    final_feature = drop_dataframe(df_)
    final_feature, d = get_categorical(final_feature)
    clf = build_model(final_feature)
    save_pickle(clf, PICKLE_MODEL)

## EXPORT PREDICTIONS TO DATAFRAME FOR BOKEH

y_pred = clf.predict(final_feature.copy().drop(['PrimaryType'], axis=1))
df_['Prediction'] = np.array([d[y] for y in y_pred])
save_pickle(df_, PICKLE_DATAFRAME)

