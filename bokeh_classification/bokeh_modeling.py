import os
import pickle
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import collections
import sqlite3

from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn import metrics
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report

WORKING_DIR = os.path.dirname(os.path.abspath(__file__))
MAP_CATEGORIES = {
    'THEFT' : 'THEFT / ROBBERY',
    'BATTERY' : 'PHYSICAL VIOLENCE',
    'CRIMINAL DAMAGE' : 'CRIMINAL DAMAGE',
    'ASSAULT' : 'PHYSICAL VIOLENCE',
    'DECEPTIVE PRACTICE' : 'STOLEN PROPERTY',
    'OTHER OFFENSE' : 'OTHER OFFENSE',
    'NARCOTICS' : 'DRUG / NARCOTIC / ALCOHOL',
    'BURGLARY' : 'BURGLARY',
    'ROBBERY' : 'THEFT / ROBBERY',
    'MOTOR VEHICLE THEFT' : 'MOTOR VEHICLE THEFT',
    'CRIMINAL TRESPASS' : 'BURGLARY',
    'WEAPONS VIOLATION' : 'WEAPONS VIOLATION',
    'OFFENSE INVOLVING CHILDREN' : 'OTHER OFFENSE',
    'PUBLIC PEACE VIOLATION' : 'OTHER OFFENSE',
    'CRIM SEXUAL ASSAULT' : 'PROSTITUTION / SEX OFFENSE',
    'INTERFERENCE WITH PUBLIC OFFICER' : 'OTHER OFFENSE',
    'SEX OFFENSE' : 'PROSTITUTION / SEX OFFENSE',
    'PROSTITUTION' : 'PROSTITUTION / SEX OFFENSE',
    'HOMICIDE' : 'OTHER OFFENSE',
    'ARSON' : 'CRIMINAL DAMAGE',
    'LIQUOR LAW VIOLATION' : 'DRUG / NARCOTIC / ALCOHOL',
    'GAMBLING' : 'OTHER OFFENSE',
    'KIDNAPPING' : 'OTHER OFFENSE',
    'STALKING'  :'OTHER OFFENSE',
    'INTIMIDATION' : 'OTHER OFFENSE',
    'CONCEALED CARRY LICENSE VIOLATION' : 'OTHER OFFENSE',
    'OBSCENITY' : 'OTHER OFFENSE',
    'NON-CRIMINAL' : 'OTHER OFFENSE',
    'PUBLIC INDECENCY' : 'OTHER OFFENSE',
    'HUMAN TRAFFICKING' : 'OTHER OFFENSE',
    'NON - CRIMINAL' : 'OTHER OFFENSE',
    'OTHER NARCOTIC VIOLATION' : 'DRUG / NARCOTIC / ALCOHOL',
    'NON-CRIMINAL (SUBJECT SPECIFIED)' : 'OTHER OFFENSE'
}
DROP_CAT = ['Beat_num', 'Ward', 'CommunityArea', 'ID',
            'CaseNumber', 'NormalizedDate',
            'Block', 'IUCR', 'Description', 'Arrest',
            'Domestic', 'FBICode', 'UpdatedOn', 'Latitude',
            'Longitude', 'Minute', 'LocationDescription']

DUMMY_FEAT = ['District', 'Month', 'Week', 'Day', 'Hour']

DB_FILENAME = 'ChicagoCrime.sqlite'

PICKLE_MODEL_RF = 'bokeh_model_rf.pkl'
PICKLE_MODEL_KNN = 'bokeh_model_knn.pkl'
PICKLE_MODEL_XGB = 'bokeh_model_xgb.pkl'
PICKLE_DATAFRAME = 'bokeh_df.pkl'
PICKLE_FEAT_IMPORTANCE = 'bokeh_feat_importance.pkl'

START_DATE = '2017-01-01'
TRAIN_TEST_DATE = '2018-09-01'

TRAIN_VLD_RATIO = 0.8

def format_dataframe(df):
    df.PrimaryType = df.PrimaryType.map(MAP_CATEGORIES)
    df = df[df.Latitude != ''][df.Longitude != ''][df.District != '']
    df = df.rename(columns={"Beat": "Beat_num"})
    df.XCoordinate = df.XCoordinate.astype("float")
    df.YCoordinate = df.YCoordinate.astype("float")
    df.Latitude = df.Latitude.astype("float")
    df.Longitude = df.Longitude.astype("float")
    df.District = df.District.astype("int")
    # Date
    df.loc[:, 'Date'] = pd.to_datetime(df.loc[:, 'Date'])
    df['Month'] = df.loc[:, 'Date'].apply(lambda x: x.month)
    df['Week'] = df.loc[:, 'Date'].apply(lambda x: x.week)
    df['Day'] = df.loc[:, 'Date'].apply(lambda x: x.weekday())
    df['Hour'] = df.loc[:, 'Date'].apply(lambda x: x.hour)
    df['Minute'] = df.loc[:, 'Date'].apply(lambda x: x.minute)
    # Retime dataframe
    df = df.sort_values(by='Date', axis=0).reset_index(drop=True)
    return df

def drop_dataframe(df):
    final_feature = df.copy()
    final_feature = final_feature.drop(DROP_CAT, axis=1)
    return final_feature

def get_categorical(final_feature):
    c = final_feature.PrimaryType.astype('category')
    d = dict(enumerate(c.cat.categories))
    for i in final_feature.columns:
        if final_feature[i].dtype == "object":
            # transform all the non-categorical data to categorical
            final_feature[i] = pd.Categorical(final_feature[i]).codes
    return final_feature, d

def drop_date_year(df, delete_test=True):
    if delete_test:
        df = df[df['Date'] < TRAIN_TEST_DATE]
    df = df.drop(['Date', 'Year'], axis = 1)
    return df

def normalize(df):
    df_ = df.copy()
    for feature_name in df.columns:
        if feature_name != 'PrimaryType':
            max_value = df[feature_name].max()
            min_value = df[feature_name].min()
            df_[feature_name] = (df[feature_name] - min_value) / (max_value - min_value)
    return df_

def balance(df):
    c = collections.Counter(df.PrimaryType)
    median_value = np.median(list(c.values()))
    for cat, val in c.items():
        if val > median_value:
            df = df.drop(df[df['PrimaryType'] == cat].sample(frac=1-median_value/val).index, axis=0).reset_index(drop=True)
    return df

def dummy(df):
    for col in DUMMY_FEAT:
        df = pd.concat([df, pd.get_dummies(df[col], prefix=col)], axis=1)
        df.drop([col], axis=1, inplace=True)
    return df

def build_model(df, algo, balanced, shuffled, cat_names):
    df_ = df.copy()

    X_train = df_.iloc[:int(TRAIN_VLD_RATIO * len(df_)), :]
    X_vld = df_.iloc[int(TRAIN_VLD_RATIO * len(df_)):, :]

    if balanced:
        X_train = balance(X_train)

    if shuffled:
        idx = np.random.permutation(X_train.index)
        X_train = X_train.reindex_axis(idx, axis=0)

    y_train = X_train.pop('PrimaryType')
    y_vld = X_vld.pop('PrimaryType')

    if algo == 'RF':
        model = RandomForestClassifier(n_estimators=10, min_samples_leaf=10, min_samples_split=50,
                                       class_weight='balanced')
        model.fit(X_train, y_train)
        y_vld_pred = model.predict(X_vld)
        print('RF Classif balanced = {}'.format(balanced))
        print(classification_report(y_vld, y_vld_pred, target_names=cat_names.values()))
    if algo == 'KNN':
        model = KNeighborsClassifier(n_neighbors=5, weights='distance')
        model.fit(X_train, y_train)
        y_vld_pred = model.predict(X_vld)
        print('KNN Classif balanced = {}'.format(balanced))
        print(classification_report(y_vld, y_vld_pred, target_names=cat_names.values()))
    if algo == 'XGB':
        model = model = GradientBoostingClassifier()
        model.fit(X_train, y_train)
        y_vld_pred = model.predict(X_vld)
        print('XGB Classif balanced = {}'.format(balanced))
        print(classification_report(y_vld, y_vld_pred, target_names=cat_names.values()))
    return model


def save_pickle(result, pickle_name):
    """Saves an object in pickle object file for future use"""
    with open(os.path.join(WORKING_DIR, pickle_name), 'wb') as file:
        pickle.dump(result, file)
        return True

def load_pickle(pickle_name):
    """Loads a previously stored model dictionary with pickle"""
    with open(os.path.join(WORKING_DIR, pickle_name), 'rb') as handle:
        return pickle.load(handle)

## LOAD FROM DATABASE ##

print('LOAD FROM DATABASE...')

with sqlite3.connect(DB_FILENAME) as con:
    cur = con.cursor()
    print("SELECT * FROM chicagoCrime WHERE Date>=date('{}');".format(START_DATE))
    result = cur.execute("SELECT * FROM chicagoCrime WHERE Date>=date('{}');".format(START_DATE))
    columns = [description[0] for description in cur.description]
    result = result.fetchall()
    cur.close()

## BUILD AND APPLY MODEL ##
print('BUILD AND APPLY MODEL...')

if os.path.exists(os.path.join(WORKING_DIR, PICKLE_MODEL_RF)) and os.path.exists(
        os.path.join(WORKING_DIR, PICKLE_MODEL_KNN)):
    clf_RF = load_pickle(PICKLE_MODEL_RF)
    clf_KNN = load_pickle(PICKLE_MODEL_KNN)
    clf_XGB = load_pickle(PICKLE_MODEL_XGB)
    df_0_0 = pd.DataFrame(data=result, columns=columns)  # raw dataframe
    df_0_0 = format_dataframe(df_0_0)  # drop empty rows, change types, parse date, order
    df_0_1 = drop_dataframe(df_0_0)  # drop unusued columns
    df_0_1, d = get_categorical(df_0_1)  # Change Labels to to int, save in dict equivalence
else:
    df_0_0 = pd.DataFrame(data=result, columns=columns)  # raw dataframe
    df_0_0 = format_dataframe(df_0_0)  # drop empty rows, change types, parse date, order
    df_0_1 = drop_dataframe(df_0_0)  # drop unusued columns
    df_0_1, d = get_categorical(df_0_1)  # Change Labels to to int, save in dict equivalence

    df_1_0 = drop_date_year(df_0_1, delete_test=True)  # Drop TEST lines for TRAIN/VLD
    # df_1_0 = dummy(df_1_0)
    df_1_0 = normalize(df_1_0)

    clf_RF = build_model(df_1_0, algo='RF', balanced=False, shuffled=False, cat_names=d)
    save_pickle(clf_RF, PICKLE_MODEL_RF)

    clf_KNN = build_model(df_1_0, algo='KNN', balanced=False, shuffled=False, cat_names=d)
    save_pickle(clf_KNN, PICKLE_MODEL_KNN)

    clf_XGB = build_model(df_1_0, algo='XGB', balanced=True, shuffled=False, cat_names=d)
    save_pickle(clf_XGB, PICKLE_MODEL_XGB)


## EXPORT PREDICTIONS TO DATAFRAME FOR BOKEH

#y_pred = clf.predict(final_feature.copy().drop(['PrimaryType'], axis=1))

df_2_0 = drop_date_year(df_0_1, delete_test=False) # Keep TEST lines and TRAIN/VLD
df_2_0 = normalize(df_2_0.drop(['PrimaryType'], axis=1))

for clf,name in [(clf_RF, 'RF'), (clf_KNN, 'KNN'), (clf_XGB, 'XGB')]:
    print('calculating preds for model {}'.format(name))
    y_probas = clf.predict_proba(df_2_0)
    y_pred = [np.argsort(y_proba)[-1:] for y_proba in y_probas]
    df_0_0['Prediction_'+name] = np.array([d[y[0]] for y in y_pred])
    if name == 'RF':
        df_0_0['Prediction'] = np.array([d[y[0]] for y in y_pred])

save_pickle(df_0_0, PICKLE_DATAFRAME)
feature_importance_df = pd.DataFrame(np.array([df_2_0.columns,
                                               np.around(clf_RF.feature_importances_, 2),
                                               np.around(clf_RF.feature_importances_, 2),
                                               np.around(clf_XGB.feature_importances_, 2)]).T,
                                     columns=['features', 'importance', 'importance_RF','importance_XGB'])
save_pickle(feature_importance_df, PICKLE_FEAT_IMPORTANCE)

