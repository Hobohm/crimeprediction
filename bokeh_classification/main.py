import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn import manifold

import os
import pickle
import sys
import sqlite3
import math
import datetime

from bokeh.palettes import Category20 as palette
import itertools
import bokeh.models as bmo
from bokeh.palettes import d3

from sklearn.metrics import confusion_matrix
import numpy as np
from bokeh.models.widgets import CheckboxGroup, MultiSelect, Select, DateRangeSlider, RangeSlider, Button, RadioGroup, MultiSelect, Div, Slider, Dropdown, Paragraph, TableColumn, DateFormatter, DataTable, DateSlider, CheckboxButtonGroup, RadioButtonGroup
from bokeh.io import curdoc
from bokeh.layouts import widgetbox
from bokeh.layouts import row, column
from bokeh.plotting import figure
from bokeh.plotting import show, ColumnDataSource
from bokeh.models import ColumnDataSource, Label, HoverTool, Range1d
from bokeh.models import LabelSet
from bokeh.models.annotations import Title

from bokeh.tile_providers import CARTODBPOSITRON
from bokeh.tile_providers import STAMEN_TONER

### GLOBAL VARIABLES ###
WORKING_DIR = os.path.dirname(os.path.abspath(__file__))
PICKLE_DF = 'bokeh_df.pkl'
PICKLE_FEAT_IMPORTANCE = 'bokeh_feat_importance.pkl'
START_DATE = "01-01-2019"
START_TEST_DATE = "01-01-2019"

### PICKLE FACTORY ###

def save_pickle(result, pickle_name):
    """Saves an object in pickle object file for future use"""
    with open(os.path.join(WORKING_DIR, pickle_name), 'wb') as file:
        pickle.dump(result, file)
        return True

def load_pickle(pickle_name):
    """Loads a previously stored model dictionary with pickle"""
    with open(os.path.join(WORKING_DIR, pickle_name), 'rb') as handle:
        return pickle.load(handle)


if os.path.exists(os.path.join(WORKING_DIR, PICKLE_DF)):
    df = load_pickle(PICKLE_DF)
else:
    print('Missing df pickle : run bokeh_modeling.py !')

if os.path.exists(os.path.join(WORKING_DIR, PICKLE_FEAT_IMPORTANCE)):
    feature_importance_df = load_pickle(PICKLE_FEAT_IMPORTANCE)
else:
    print('Missing feat_importance pickle : run bokeh_modeling.py !')


def merc(Coords):
    lat = Coords[0]
    lon = Coords[1]

    r_major = 6378137.000
    x = r_major * math.radians(lon)
    scale = x / lon
    y = 180.0 / math.pi * math.log(math.tan(math.pi / 4.0 +
                                            lat * (math.pi / 180.0) / 2.0)) * scale
    return (x, y)


df['coords_x'] = df.apply(lambda x: merc((x.Latitude, x.Longitude))[0], axis=1)
df['coords_y'] = df.apply(lambda x: merc((x.Latitude, x.Longitude))[1], axis=1)

df = df[df['Date'] >= datetime.datetime.strptime(START_DATE, "%d-%m-%Y")]


## DATA SOURCE ##

source = ColumnDataSource(df)
source_predict = ColumnDataSource(df)
source_both = ColumnDataSource(pd.DataFrame(columns=df.columns))
source_metrics_RF = ColumnDataSource(data=dict(category=[], precision=[], recall=[], f1=[], size_T=[], size_P=[]))
source_feat = ColumnDataSource(feature_importance_df)

columns_metrics_RF = [
    TableColumn(field="category", title="Random Forest"),
    TableColumn(field="precision", title="precision"),
    TableColumn(field="recall", title="recall"),
    TableColumn(field="f1", title="f1"),
    TableColumn(field="size_T", title="size_T"),
    TableColumn(field="size_P", title="size_P")
        ]
source_metrics_KNN = ColumnDataSource(data=dict(category=[], precision=[], recall=[], f1=[], size_T=[], size_P=[]))
columns_metrics_KNN = [
    TableColumn(field="category", title="KNN"),
    TableColumn(field="precision", title="precision"),
    TableColumn(field="recall", title="recall"),
    TableColumn(field="f1", title="f1"),
    TableColumn(field="size_T", title="size_T"),
    TableColumn(field="size_P", title="size_P")
        ]
source_metrics_XGB = ColumnDataSource(data=dict(category=[], precision=[], recall=[], f1=[], size_T=[], size_P=[]))
columns_metrics_XGB = [
    TableColumn(field="category", title="XGB"),
    TableColumn(field="precision", title="precision"),
    TableColumn(field="recall", title="recall"),
    TableColumn(field="f1", title="f1"),
    TableColumn(field="size_T", title="size_T"),
    TableColumn(field="size_P", title="size_P")
        ]
source_metrics_display = ColumnDataSource(data=dict(category=[], precision=[], recall=[], f1=[], size_T=[], size_P=[]))
columns_metrics_display = [
    TableColumn(field="category", title="Label"),
    TableColumn(field="precision", title="precision"),
    TableColumn(field="recall", title="recall"),
    TableColumn(field="f1", title="f1"),
    TableColumn(field="size_T", title="size_T"),
    TableColumn(field="size_P", title="size_P")
        ]
table_metrics_RF = DataTable(source=source_metrics_RF, columns=columns_metrics_RF, width=530, height=280, sortable=True, fit_columns=True)
table_metrics_KNN = DataTable(source=source_metrics_KNN, columns=columns_metrics_KNN, width=530, height=280, sortable=True, fit_columns=True)
table_metrics_XGB = DataTable(source=source_metrics_XGB, columns=columns_metrics_XGB, width=530, height=280, sortable=True, fit_columns=True)
table_metrics_display = DataTable(source=source_metrics_display, columns=columns_metrics_display, width=530, height=50, sortable=True, fit_columns=True)

## MODELS ##

#model_menu = [("Random Forest", 'Prediction_RF'), None, ("K-Nearest-Neighbors", 'Prediction_KNN')]
#model_dropdown = Dropdown(label="Model", button_type="warning", menu=model_menu, value='Prediction_RF')
model_menu = [('Prediction_RF', "Random Forest"), ('Prediction_KNN', "K-Nearest Neighbors"), ('Prediction_XGB', "XG Boost")]
model_dropdown = Select(title="Model", options=model_menu, value='Prediction_RF')

## IMGS ##
logo_chicago = os.path.join(os.path.basename(os.path.dirname(__file__)), "static", "chicago.png")
logo_src_1 = ColumnDataSource(dict(url=[logo_chicago]))
image1 = figure(plot_width=100, plot_height=100, title="")
image1.toolbar.logo = None
image1.toolbar_location = None
image1.x_range = Range1d(start=0, end=1)
image1.y_range = Range1d(start=0, end=1)
image1.xaxis.visible = None
image1.yaxis.visible = None
image1.xgrid.grid_line_color = None
image1.ygrid.grid_line_color = None
image1.image_url(url='url', x=0, y=1, h=1, w=1, source=logo_src_1)
image1.outline_line_alpha = 0

logo_itc = os.path.join(os.path.basename(os.path.dirname(__file__)), "static", "itc.png")
logo_src_2 = ColumnDataSource(dict(url=[logo_itc]))
image2 = figure(plot_width=100, plot_height=100, title="")
image2.toolbar.logo = None
image2.toolbar_location = None
image2.x_range=Range1d(start=0, end=1)
image2.y_range=Range1d(start=0, end=1)
image2.xaxis.visible = None
image2.yaxis.visible = None
image2.xgrid.grid_line_color = None
image2.ygrid.grid_line_color = None
image2.image_url(url='url', x=0, y=1, h=1, w=1, source=logo_src_2)
image2.outline_line_alpha = 0

logo_city = os.path.join(os.path.basename(os.path.dirname(__file__)), "static", "batman2.jpg")
logo_src_3 = ColumnDataSource(dict(url=[logo_city]))
image3 = figure(plot_width=100, plot_height=100, title="")
image3.toolbar.logo = None
image3.toolbar_location = None
image3.x_range=Range1d(start=0, end=1)
image3.y_range=Range1d(start=0, end=1)
image3.xaxis.visible = None
image3.yaxis.visible = None
image3.xgrid.grid_line_color = None
image3.ygrid.grid_line_color = None
image3.image_url(url='url', x=0, y=1, h=1, w=1, source=logo_src_3)
image3.outline_line_alpha = 0

## SLIDERS ##

date_ = DateRangeSlider(title='Date',
                        value=(datetime.datetime.strptime(START_TEST_DATE, "%d-%m-%Y"), max(df['Date'])),
                        start=min(df['Date']),
                        end=max(df['Date']),
                        step=1)

hour_ = RangeSlider(title='Hour',
                        value=(min(df['Hour']), max(df['Hour'])),
                        start=min(df['Hour']),
                        end=max(df['Hour']),
                        step=1)

## DAY OF WEEK ##
day_of_week = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
#days = CheckboxButtonGroup(labels=day_of_week, active=list(range(len(day_of_week))))
days = MultiSelect(title='Day of Week',
                   size=7,
                   value=[str(i) for i in range(len(day_of_week))],
                   options=[(str(i), day) for i, day in enumerate(day_of_week)])

# SELECT DISTRICTS
district_list = [str(x) for x in sorted(pd.unique(df['District'].values))]
districts = MultiSelect(title='Districts',
                        size=5,
                        value=district_list,
                        options=[(str(district), district) for district in district_list])

## TEXT ##
main_title = Div(text="<b>Chicago crime prediction : classification</b>", style={'font-size': '150%'}, width=600, height=30)

## FEATURE IMPORTANCE ##
feat_RF_plot = figure(plot_width=580, plot_height=150, title="Feature importance Random Forest", x_range=feature_importance_df['features'])
feat_RF_plot.vbar(source=source_feat, x='features', top='importance_RF', width=0.9)
feat_XGB_plot = figure(plot_width=580, plot_height=150, title="Feature importance XG Boost", x_range=feature_importance_df['features'])
feat_XGB_plot.vbar(source=source_feat, x='features', top='importance_XGB', width=0.9)

categories = list(pd.unique(df['PrimaryType'].values))
#type_of_crime = CheckboxButtonGroup(labels=categories, active=[4])
type_of_crime = MultiSelect(title='Labels',
                            size=10,
                            value=['5'],
                            options=[(str(i), label) for i, label in enumerate(categories)])
#prediction_checkbox = RadioButtonGroup(labels=['Data', 'Show Correct'], active=0)
prediction_checkbox = CheckboxGroup(labels=['Display correct predictions'], active=[])

p1 = figure(plot_width=580, plot_height=560, x_axis_type="mercator", y_axis_type="mercator", title=Title(text='Labels'), toolbar_location="below")
p1.title.text_font_size = '16pt'
p1.add_tile(STAMEN_TONER)
p1.axis.visible = False
p1.outline_line_width = 5
p1.outline_line_alpha = 0.3
p1.outline_line_color = "navy"

p2 = figure(plot_width=580, plot_height=560, x_axis_type="mercator", y_axis_type="mercator", title=Title(text='Predictions'), toolbar_location="below")
p2.title.text_font_size = '16pt'
p2.add_tile(STAMEN_TONER)
p2.axis.visible = False
p2.outline_line_width = 5
p2.outline_line_alpha = 0.3
p2.outline_line_color = "navy"

# create a color iterator
colors_no_green = palette[20][:4] + palette[20][6:]
color_map = bmo.CategoricalColorMapper(factors=pd.unique(df['PrimaryType'].values), palette=colors_no_green)

p1.scatter(source=source,
          x='coords_x',
          y='coords_y',
          #radius = 's',
          legend='PrimaryType',
          color={'field': 'PrimaryType', 'transform': color_map},
          marker="circle",
          alpha=0.75)

p1.scatter(source=source_both,
          x='coords_x',
          y='coords_y',
          #radius = 's',
          legend=False,
          line_color='green', fill_color="green",
          marker="circle",
          alpha=0.50)

p2.scatter(source=source_predict,
          x='coords_x',
          y='coords_y',
          #radius = 's',
          legend='Prediction',
          color={'field': 'Prediction', 'transform': color_map},
          marker="circle",
          alpha=0.75)

p2.scatter(source=source_both,
          x='coords_x',
          y='coords_y',
          #radius = 's',
          legend=False,
           line_color='green', fill_color="green",
          marker="circle",
          alpha=0.50)

p2.legend.click_policy="hide"
p1.legend.click_policy="hide"


## DEFINE CALLBACK UPDATE ##

def update_data(attrname, old, new):
    start_date_, end_date_ = date_.value_as_datetime
    start_hour_, end_hour_ = hour_.value
    #category_index = type_of_crime.active
    category_index = [int(i) for i in type_of_crime.value]
    labeled_only = prediction_checkbox.active
    district_selected = [int(i) for i in districts.value]
    days_selected = [int(i) for i in days.value]
    prediction_col = model_dropdown.value

    df_ = df[df['Date'] >= start_date_][df['Date'] <= end_date_][df['Hour'] >= start_hour_][df['Hour'] <= end_hour_][df['District'].isin(district_selected)][df['Day'].isin(days_selected)]
    df_['Prediction'] = df_[prediction_col]

    source.data = source.from_df(
        df_[df_['PrimaryType'].isin([categories[i] for i in category_index])]
    )

    source_predict.data = source_predict.from_df(
        df_[df_['Prediction'].isin([categories[i] for i in category_index])]
    )

    if len(labeled_only) == 0:
        #empty data source
        source_both.data = source_predict.from_df(pd.DataFrame(columns=df_.columns))
    else:
        df__ = df_[df_['Prediction'].isin([categories[i] for i in category_index])][df_['PrimaryType'] == df_['Prediction']]
        source_both.data = source_both.from_df(df__)

    df__RF = df_[df_['PrimaryType'] == df_['Prediction_RF']]
    df__KNN = df_[df_['PrimaryType'] == df_['Prediction_KNN']]
    df__XGB = df_[df_['PrimaryType'] == df_['Prediction_XGB']]
    df__display = df_[df_['PrimaryType'] == df_['Prediction']]

    data_metrics_RF = {'category': [], 'precision': [], 'recall': [], 'f1': [], 'size_T' : [], 'size_P' : []}
    data_metrics_KNN = {'category': [], 'precision': [], 'recall': [], 'f1': [], 'size_T' : [], 'size_P' : []}
    data_metrics_XGB = {'category': [], 'precision': [], 'recall': [], 'f1': [], 'size_T': [], 'size_P': []}
    data_metrics_display = {'category': [], 'precision': [], 'recall': [], 'f1': [], 'size_T': [], 'size_P': []}

    for i,category in enumerate(list(pd.unique(df['PrimaryType'].values))):

        size_T = round(len(df_[df_['PrimaryType'] == category]) / len(df_), 2)

        recall_RF = round(len(df__RF[df__RF['PrimaryType'] == category])/len(df_[df_['PrimaryType'] == category]), 2)
        precision_RF = round(len(df__RF[df__RF['Prediction_RF'] == category])/len(df_[df_['Prediction_RF'] == category]), 2)
        f1_RF = round(2 * precision_RF * recall_RF / (precision_RF + recall_RF + 1e-6), 2)
        size_P_RF = round(len(df__RF[df__RF['Prediction_RF'] == category]) / len(df__RF), 2)

        recall_KNN = round(len(df__KNN[df__KNN['PrimaryType'] == category]) / len(df_[df_['PrimaryType'] == category]), 2)
        precision_KNN = round(len(df__KNN[df__KNN['Prediction_KNN'] == category]) / len(df_[df_['Prediction_KNN'] == category]), 2)
        f1_KNN = round(2 * precision_KNN * recall_KNN / (precision_KNN + recall_KNN + 1e-6), 2)
        size_P_KNN = round(len(df__KNN[df__KNN['Prediction_KNN'] == category]) / len(df__KNN), 2)

        recall_XGB = round(len(df__XGB[df__XGB['PrimaryType'] == category]) / len(df_[df_['PrimaryType'] == category]), 2)
        precision_XGB = round(len(df__XGB[df__XGB['Prediction_XGB'] == category]) / len(df_[df_['Prediction_XGB'] == category]), 2)
        f1_XGB = round(2 * precision_XGB * recall_XGB / (precision_XGB + recall_XGB + 1e-6), 2)
        size_P_XGB = round(len(df__XGB[df__XGB['Prediction_XGB'] == category]) / len(df__XGB), 2)

        if category in [categories[j] for j in category_index]:
            recall_display = round(len(df__display[df__display['PrimaryType'] == category]) / len(df_[df_['PrimaryType'] == category]), 2)
            precision_display = round(len(df__display[df__display['Prediction'] == category]) / len(df_[df_['Prediction'] == category]), 2)
            f1_display = round(2 * precision_display * recall_display / (precision_display + recall_display + 1e-6), 2)
            size_P_display = round(len(df__display[df__display['Prediction'] == category]) / len(df__display), 2)
            data_metrics_display['category'].append(category)
            data_metrics_display['precision'].append(precision_display)
            data_metrics_display['recall'].append(recall_display)
            data_metrics_display['f1'].append(f1_display)
            data_metrics_display['size_T'].append(size_T)
            data_metrics_display['size_P'].append(size_P_display)

        data_metrics_RF['category'].append(category)
        data_metrics_RF['precision'].append(precision_RF)
        data_metrics_RF['recall'].append(recall_RF)
        data_metrics_RF['f1'].append(f1_RF)
        data_metrics_RF['size_T'].append(size_T)
        data_metrics_RF['size_P'].append(size_P_RF)

        data_metrics_KNN['category'].append(category)
        data_metrics_KNN['precision'].append(precision_KNN)
        data_metrics_KNN['recall'].append(recall_KNN)
        data_metrics_KNN['f1'].append(f1_KNN)
        data_metrics_KNN['size_T'].append(size_T)
        data_metrics_KNN['size_P'].append(size_P_KNN)

        data_metrics_XGB['category'].append(category)
        data_metrics_XGB['precision'].append(precision_XGB)
        data_metrics_XGB['recall'].append(recall_XGB)
        data_metrics_XGB['f1'].append(f1_XGB)
        data_metrics_XGB['size_T'].append(size_T)
        data_metrics_XGB['size_P'].append(size_P_XGB)

    source_metrics_RF.data = source_metrics_RF.from_df(pd.DataFrame(data=data_metrics_RF))
    source_metrics_KNN.data = source_metrics_KNN.from_df(pd.DataFrame(data=data_metrics_KNN))
    source_metrics_XGB.data = source_metrics_XGB.from_df(pd.DataFrame(data=data_metrics_XGB))
    source_metrics_display.data = source_metrics_display.from_df(pd.DataFrame(data=data_metrics_display))

update_data(None, None, None)

date_.on_change('value', update_data)
hour_.on_change('value', update_data)

#type_of_crime.on_change('active', update_data)
type_of_crime.on_change('value', update_data)
prediction_checkbox.on_change('active', update_data)

#days.on_change('active', update_data)
#districts.on_change('active', update_data)
days.on_change('value', update_data)
districts.on_change('value', update_data)

model_dropdown.on_change('value', update_data)

#curdoc().add_root(column(row(date_, days, districts, image1, image2), hour_, row(column(type_of_crime, model_dropdown, prediction_checkbox, output_analytics), p1, p2)))
curdoc().add_root(
    column(
        row(image1, image2, image3, column(main_title, prediction_checkbox), table_metrics_display), row
        (
            column(model_dropdown, type_of_crime, date_, hour_, days, districts),
            column(p1, table_metrics_RF, feat_RF_plot, feat_XGB_plot),
            column(p2, table_metrics_XGB, table_metrics_KNN)
        )
    ))

curdoc().title = "Chicago map"