"""Main file for calculating Chicago Regression"""

import ChicagoClasses
import re
import pandas as pd
from sklearn.linear_model import LinearRegression
from sklearn.svm import LinearSVR
import sqlite3
import numpy as np
import pickle
import os
import requests
from bs4 import BeautifulSoup
import datetime as dt

SQL_FILE = r'..\..\CnvrgData\ChicagoCrime.sqlite'
DATE_COLS = ['Date', 'UpdatedOn', 'NormalizedDate']


def load_data(sql_file=SQL_FILE, date_cols=DATE_COLS):
    with sqlite3.connect(sql_file) as con:
        df = pd.read_sql_query(
            """ SELECT * from chicagoCrime where CAST(strftime('%Y', NormalizedDate) as INTEGER) >= 2015""",
            con, parse_dates=date_cols)
    return df


def format_dataframe(df):
    """Format the columns in the dataframe"""
    df['Arrest'] = df.Arrest.apply(lambda x: 1 if x == 'true' else 0)
    df['Domestic'] = df.Domestic.apply(lambda x: 1 if x == 'true' else 0)
    df['Latitude'] = df.Latitude.apply(lambda x: float(x) if x != '' else None)
    df['Longitude'] = df.Longitude.apply(lambda x: float(x) if x != '' else None)
    df['District'] = df.District.apply(lambda x: int(x) if x != '' else None)
    df['Ward'] = df.Ward.apply(lambda x: int(x) if x != '' else None)
    df['CommunityArea'] = df.Ward.apply(lambda x: x if x != '' else None)
    df['XCoordinate'] = df.XCoordinate.apply(lambda x: x if x != '' else None)
    df['YCoordinate'] = df.YCoordinate.apply(lambda x: x if x != '' else None)
    # get rid of NA and district 31
    filtered_df = df.dropna()
    filtered_df = filtered_df.loc[filtered_df.District != 31, :]
    return filtered_df


def save_df(df, pickle_file):
    """Save a pickle object with a given name"""
    pickle.dump(df, open(pickle_file, 'wb'))


def retrieve_data(dataframe_file, movies=False):
    """Runs the main project"""
    file_directory = os.path.dirname(os.path.abspath(__file__))
    model_file = os.path.join(file_directory, dataframe_file)
    if os.path.exists(model_file):
        df = pickle.load(open(model_file, 'rb'))
    elif not movies:
        df = load_data()
        df = format_dataframe(df)
        save_df(df, dataframe_file)
    elif movies:
        df = movie_release()
        save_df(df, dataframe_file)
    return df


def movie_release():
    """Retrieve movie release nationwide"""
    movie_release_dates = []
    movie_released = []
    for movie_year in range(2015, 2020):
        url = r'https://www.firstshowing.net/schedule' + str(movie_year) + "/"
        r = requests.get(url)
        print("Retrieving url {}...".format(url))
        soup = BeautifulSoup(r.content, 'html.parser')
        movie_table = soup.find('div', class_='schedcontent')
        movie_dates = movie_table.select('h4')[:-1]
        movies = movie_table.select('p.sched')
        assert len(movie_dates) == len(movies)
        for i in range(len(movie_dates)):
            movie_date = movie_dates[i].get_text()
            the_movies = movies[i].select('a strong')
            the_movies = [x.get_text() for x in the_movies]
            # format date - Month - day - day of week
            movie_date = movie_date.split()
            formatted_date = "{} {} {}".format(movie_date[1], movie_date[0], movie_year)
            movie_date = dt.datetime.strptime(formatted_date, r'%d %B %Y')
            for j in the_movies:
                movie_release_dates.append(movie_date)
                movie_released.append(j)
    movie_df = pd.DataFrame({'ReleaseDate': movie_release_dates, 'ReleasedMovie': movie_released})
    return movie_df


def model_create_add(district, model_name, model, model_type, features=30):
    """Records the model results, model is not kept for space reasons"""
    district_model = ChicagoClasses.DistrictModel(model_name, model, model_type,
                               district.X_train, district.Y_train,
                               district.X_test, district.Y_test, features)
    district_model.generate_linear_models()
    district.district_models.append(district_model)
    district_model.clear_model()


def model_combos(district, model, model_type, basics=False, day_avg=30, days_in_test=180):
    # reg = LinearRegression()
    # tree = ExtraTreesClassifier(n_estimators=50, criterion='entropy', random_state=42)
    # svr = LinearSVR(random_state=42, max_iter=1000, fit_intercept=False, loss='squared_epsilon_insensitive', dual=False)
    district.n_day_average(day_avg)
    district.generate_features(30)
    ### Linear Only
    district.split_data(days_in_test)
    model_create_add(district, model_type + ' - Past Days only', model, model_type, 30)
    ### Simple Model
    if basics:
        district_model = ChicagoClasses.DistrictModel('Simplest Model', model, model_type,
                                   district.X_train, district.Y_train,
                                   district.X_test, district.Y_test, 30)
        district_model.simplest_model()
        district.district_models.append(district_model)
    ### Add Holiday Flag
    district.holiday_feature(True)
    district.split_data(days_in_test)
    model_create_add(district, model_type + ' - Is Holiday', model, model_type, 30)
    ### Add Movie Feature
    district.movie_feature()
    district.split_data(days_in_test)
    model_create_add(district, model_type + ' - Is Holiday W/ Movie', model, model_type, 30)
    ### Add Dates
    district.generate_features(30)
    district.holiday_feature(True)
    district.date_features()
    district.split_data(days_in_test)
    model_create_add(district, model_type + ' - Is Holiday w/ Dates', model, model_type, 30)
    ### Add Movie Feature
    district.movie_feature()
    district.split_data(days_in_test)
    model_create_add(district, model_type + ' - Is Holiday w/ Dates Movie', model, model_type, 30)
    ### Reset with each Holiday separate
    district.generate_features(30)
    district.holiday_feature(False)
    district.split_data(days_in_test)
    model_create_add(district, model_type + ' - Each Holiday', model, model_type, 30)
    ### Add Movie Feature
    district.movie_feature()
    district.split_data(days_in_test)
    model_create_add(district, model_type + ' - Each Holiday w/ Movie', model, model_type, 30)
    ### Add Date Features
    district.generate_features(30)
    district.holiday_feature(False)
    district.date_features()
    district.split_data(days_in_test)
    model_create_add(district, model_type + ' - Each Holiday w/ Dates', model, model_type, 30)
    ### Add Movie Feature
    district.movie_feature()
    district.split_data(days_in_test)
    model_create_add(district, model_type + ' - Each Holiday w/ Dates Movie', model, model_type, 30)


def model_past_test(district, model, model_type, day_avg=30, days_in_test=180):
    # reg = LinearRegression()
    district.n_day_average(day_avg)
    district.generate_features(30)
    ### Add Holiday Flag
    district.holiday_feature(True, None, 4)
    district.split_data(days_in_test)
    model_create_add(district, model_type + ' - Is Holiday w/ Past', model, model_type, 30)
    ### Add Movie Feature
    district.movie_feature()
    district.split_data(days_in_test)
    model_create_add(district, model_type + ' - Is Holiday w/ Movie Past', model, model_type, 30)
    ### Add Dates
    district.generate_features(30)
    district.holiday_feature(True, None, 4)
    district.date_features()
    district.split_data(days_in_test)
    model_create_add(district, model_type + ' - Is Holiday w/ Dates Past', model, model_type, 30)
    ### Add Movie Feature
    district.movie_feature()
    district.split_data(days_in_test)
    model_create_add(district, model_type + ' - Is Holiday w/ Dates Movie Past', model, model_type, 30)
    ### Reset with each Holiday separate
    district.generate_features(30)
    district.holiday_feature(False, None, 4)
    district.split_data(days_in_test)
    model_create_add(district, model_type + ' - Each Holiday w/ Past', model, model_type, 30)
    ### Add Movie Feature
    district.movie_feature()
    district.split_data(days_in_test)
    model_create_add(district, model_type + ' - Each Holiday w/ Movie Past', model, model_type, 30)
    ### Add Date Features
    district.generate_features(30)
    district.holiday_feature(False, None, 4)
    district.date_features()
    district.split_data(days_in_test)
    model_create_add(district, model_type + ' - Each Holiday w/ Dates Past', model, model_type, 30)
    ### Add Movie Feature
    district.movie_feature()
    district.split_data(days_in_test)
    model_create_add(district, model_type + ' - Each Holiday w/ Dates Movie Past', model, model_type, 30)


def model_weather_test(district, model, model_type, day_avg=30, days_in_test=180):
    # reg = LinearRegression()
    district.n_day_average(day_avg)
    district.generate_features(30)
    ### Add Holiday Flag
    district.holiday_feature(True, None)
    district.weather_feature()
    district.split_data(days_in_test)
    model_create_add(district, model_type + ' - Is Holiday w/ Weather', model, model_type, 30)
    ### Add Movie Feature
    district.movie_feature()
    district.split_data(days_in_test)
    model_create_add(district, model_type + ' - Is Holiday w/ Movie Weather', model, model_type, 30)
    ### Add Dates
    district.generate_features(30)
    district.holiday_feature(True, None)
    district.date_features()
    district.weather_feature()
    district.split_data(days_in_test)
    model_create_add(district, model_type + ' - Is Holiday w/ Dates Weather', model, model_type, 30)
    ### Add Movie Feature
    district.movie_feature()
    district.split_data(days_in_test)
    model_create_add(district, model_type + ' - Is Holiday w/ Dates Movie Weather', model, model_type, 30)
    ### Reset with each Holiday separate
    district.generate_features(30)
    district.holiday_feature(False, None)
    district.weather_feature()
    district.split_data(days_in_test)
    model_create_add(district, model_type + ' - Each Holiday w/ Weather', model, model_type, 30)
    ### Add Movie Feature
    district.movie_feature()
    district.split_data(days_in_test)
    model_create_add(district, model_type + ' - Each Holiday w/ Movie Weather', model, model_type, 30)
    ### Add Date Features
    district.generate_features(30)
    district.holiday_feature(False, None)
    district.date_features()
    district.weather_feature()
    district.split_data(days_in_test)
    model_create_add(district, model_type + ' - Each Holiday w/ Dates Weather', model, model_type, 30)
    ### Add Movie Feature
    district.movie_feature()
    district.split_data(days_in_test)
    model_create_add(district, model_type + ' - Each Holiday w/ Dates Movie Weather', model, model_type, 30)


def model_past_weather_test(district, model, model_type, day_avg=30, days_in_test=180):
    # reg = LinearRegression()
    district.n_day_average(day_avg)
    district.generate_features(30)
    ### Add Holiday Flag
    district.holiday_feature(True, None, 4)
    district.weather_feature()
    district.split_data(days_in_test)
    model_create_add(district, model_type + ' - Is Holiday w/ Past Weather', model, model_type, 30)
    ### Add Movie Feature
    district.movie_feature()
    district.split_data(days_in_test)
    model_create_add(district, model_type + ' - Is Holiday w/ Movie Past Weather', model, model_type, 30)
    ### Add Dates
    district.generate_features(30)
    district.holiday_feature(True, None, 4)
    district.date_features()
    district.weather_feature()
    district.split_data(days_in_test)
    model_create_add(district, model_type + ' - Is Holiday w/ Dates Past Weather', model, model_type, 30)
    ### Add Movie Feature
    district.movie_feature()
    district.split_data(days_in_test)
    model_create_add(district, model_type + ' - Is Holiday w/ Dates Movie Past Weather', model, model_type, 30)
    ### Reset with each Holiday separate
    district.generate_features(30)
    district.holiday_feature(False, None, 4)
    district.weather_feature()
    district.split_data(days_in_test)
    model_create_add(district, model_type + ' - Each Holiday w/ Past Weather', model, model_type, 30)
    ### Add Movie Feature
    district.movie_feature()
    district.split_data(days_in_test)
    model_create_add(district, model_type + ' - Each Holiday w/ Movie Past Weather', model, model_type, 30)
    ### Add Date Features
    district.generate_features(30)
    district.holiday_feature(False, None, 4)
    district.date_features()
    district.weather_feature()
    district.split_data(days_in_test)
    model_create_add(district, model_type + ' - Each Holiday w/ Dates Past Weather', model, model_type, 30)
    ### Add Movie Feature
    district.movie_feature()
    district.split_data(days_in_test)
    model_create_add(district, model_type + ' - Each Holiday w/ Dates Movie Past Weather', model, model_type, 30)


def locate_anomalies(my_series, g_threshold, l_threshold):
    """Returns indexes of items that are outside the thresholds"""
    items_greater = [i for i in range(len(my_series)) if my_series[i] > g_threshold]
    items_less = [i for i in range(len(my_series)) if my_series[i] < l_threshold]
    return set(items_greater + items_less)


def set_anomalies(values):
    """Returns indices of items that are more than 3 std from the mean"""
    iteration = 0
    mean = np.mean(values)
    std = np.std(values)
    g_threshold = mean + 3 * std
    l_threshold = mean - 3 * std
    anoms = locate_anomalies(values, g_threshold, l_threshold)
    prev_anoms = set()
    anom_iteration = list()
    while len(anoms) > len(prev_anoms):
        anom_iteration.append(anoms)
        prev_anoms = anoms
        iteration += 1
        new_values = [v for i, v in enumerate(values) if i not in anoms]
        mean = np.mean(new_values)
        std = np.std(new_values)
        g_threshold = mean + 3 * std
        l_threshold = mean - 3 * std
        anoms = locate_anomalies(values, g_threshold, l_threshold)
    return anoms


def evaluate_districts(city_crime, test_days):
    city_data = []
    city_features = []
    for district in city_crime.districts:
        print(district.name)
        reg = LinearRegression()
        model_combos(district, reg, 'Linear', True, 30, test_days)
        model_past_test(district, reg, 'Linear', 30, test_days)
        model_past_weather_test(district, reg, 'Linear', 30, test_days)
        model_weather_test(district, reg, 'Linear', 30, test_days)
        print("  Linear Complete...")
        svr = LinearSVR(random_state=42, max_iter=1000, fit_intercept=False, loss='squared_epsilon_insensitive',
                        dual=False)
        model_combos(district, svr, 'SVR', False, 30, test_days)
        model_past_test(district, svr, 'SVR', 30, test_days)
        model_past_weather_test(district, svr, 'SVR', 30, test_days)
        model_weather_test(district, svr, 'SVR', 30, test_days)
        # Restrict to only the best model for that district
        district.keep_top_models(1)
        # Build Data Frame of Data for District
        top_model = district.district_models[0]
        district_info = {'NormalizedDate': district.test_date,
                         'District': [district.name]*len(district.test_date),
                         'CrimeActual': district.daily_crime[-test_days:]}
        if top_model.best_feature is not None:
            district_info['CrimePred'] = [round(p + t, 0) for p, t in zip(top_model.features[top_model.best_feature]['test_prediction'],
                                                                district.day_average[-test_days:])]
            district_features = sorted([(k, v) for k, v in zip(top_model.features[top_model.best_feature]['features'],
                                                  top_model.features[top_model.best_feature]['feature_importance'])],
                                       key=lambda x: abs(x[1]), reverse=True)
        else:
            district_info['CrimePred'] = [p + t for p, t in zip(top_model.features['test_prediction'],
                                                                district.day_average[-test_days:])]
            district_features = [('feature', top_model.features['feature_importance'])]
        # Add Error Values
        district_info['AbsoluteError'] = [a-p for p, a in zip(district_info['CrimePred'], district.daily_crime[-test_days:])]
        district_info['RelativeError'] = [(a-p)/a if a != 0 else 0 for p, a in zip(district_info['CrimePred'],
                                                                                   district.daily_crime[-test_days:])]
        # Add Column indicting if the value is an anamoly
        anoms = set_anomalies(district_info['AbsoluteError'])
        district_info['AbsoluteAnom'] = [1 if i in anoms else 0 for i in range(len(district.Y_test))]
        anoms = set_anomalies(district_info['RelativeError'])
        district_info['RelativeAnom'] = [1 if i in anoms else 0 for i in range(len(district.Y_test))]
        city_data.append(pd.DataFrame(district_info))
        city_features.append([district.name, district_features, top_model.name, top_model.model_type, top_model.best_feature])
    return pd.concat(city_data, ignore_index=True, axis=0), city_features


def main():
    df = retrieve_data('dataframe.sav')
    movie_df = retrieve_data('ReleasedMovies.pkl', True)
    # Create the city of chicago and each district
    city_crime = ChicagoClasses.ChicagoCrime(df)
    city_crime.create_districts()
    city_predictions, city_features =  evaluate_districts(city_crime, 180)
    city_predictions['DistrictNum'] = city_predictions.District.apply(lambda x: int(re.search(r'[\d]+', x).group()))
    save_df([city_predictions, city_features], "BestModels.pkl")


if __name__ == '__main__':
    main()
