"""File for regression analysis and capturing district features"""


import numpy as np
from matplotlib import pyplot as plt
from sklearn.metrics import mean_squared_error
import collections
import pandas as pd
import pickle


class ChicagoCrime:
    def __init__(self, data):
        self.districts = []
        self.df = data
        self.best_models = []

    def create_districts(self):
        """For each district add to Districts property once created"""
        districts = self.df.District.unique()
        for d in districts:
            d_name = "District" + str(int(d))
            d_data = self.df.loc[self.df.District == d, :]
            new_district = DistrictCrime(d_name, d_data)
            self.districts.append(new_district)

    def generate_best_models(self, model_type=None, print_items=False):
        """For each district pring out the model that had the  lowest mse"""
        for district in self.districts:
            model_names, model_mse, model_pred, model_features = district.model_report(model_type)
            model_rank = sorted([(n, m, f, p) for n, m, f, p in zip(model_names, model_mse,
                                                                    model_features, model_pred)], key=lambda x: x[1])
            if print_items:
                print(district.name)
                print("- Model: {} - mse: {}".format(model_rank[0][0], model_rank[0][1]), max(model_mse))
            self.best_models.append([district, model_rank[0]])


class DistrictCrime:
    def __init__(self, name, data):
        self.name = name
        self.data = data
        self.daily_crime = data.groupby(['NormalizedDate']).ID.count()
        self.dates = self.daily_crime.index
        # Properties for Later
        self.day_average = None
        self.day_average_days = None
        self.detrended = None
        self.anomaly_index = None
        # Feature Properties
        self.crime_features = None
        self.true_crime = None
        self.model_dates = None
        self.days_of_past_crime = None
        # Model Input
        self.X_train = None
        self.Y_train = None
        self.train_date = None
        self.X_test = None
        self.Y_test = None
        self.test_date = None
        self.district_models = []

    def n_day_average(self, days):
        """Sets properties that contain a n-day average and how much n is"""
        day_average = [np.mean(self.daily_crime[i - days: i]) for i in range(len(self.daily_crime)) if i > days - 1]
        self.day_average = day_average
        self.day_average_days = days
        self.detrend_crime()
        self.set_anomalies(self.detrended)

    def detrend_crime(self):
        """Set the detrended variable"""
        detrended = [actual - trend for actual, trend in
                     zip(self.daily_crime[self.day_average_days:], self.day_average)]
        self.detrended = detrended

    # Anomaly Section
    @staticmethod
    def locate_anomalies(my_series, g_threshold, l_threshold):
        items_greater = [i for i in range(len(my_series)) if my_series[i] > g_threshold]
        items_less = [i for i in range(len(my_series)) if my_series[i] < l_threshold]
        return set(items_greater + items_less)

    def set_anomalies(self, values):
        iteration = 0
        mean = np.mean(values)
        std = np.std(values)
        g_threshold = mean + 3 * std
        l_threshold = mean - 3 * std
        anoms = self.locate_anomalies(values, g_threshold, l_threshold)
        prev_anoms = set()
        anom_iteration = list()
        while len(anoms) > len(prev_anoms):
            anom_iteration.append(anoms)
            prev_anoms = anoms
            iteration += 1
            new_values = [v for i, v in enumerate(values) if i not in anoms]
            mean = np.mean(new_values)
            std = np.std(new_values)
            g_threshold = mean + 3 * std
            l_threshold = mean - 3 * std
            anoms = self.locate_anomalies(values, g_threshold, l_threshold)
        self.anomaly_index = anoms

    def min_max_scaler(self, fields, col_min, col_max):
        fields_new = [(r - col_min) / (col_max - col_min) for r in fields]
        return fields_new

    # Get Crime Features
    def generate_features(self, num_fea):
        """On detrended series, create a table such that for each day, the previous k day values are in the row."""
        # crime_features = [self.detrended[i - num_fea:i] for i in range(len(self.detrended)) if i >= num_fea]
        crime_true = [self.detrended[i] for i in range(len(self.detrended)) if i >= num_fea]
        crime_date = [d for i, d in enumerate(self.dates[self.day_average_days:]) if i >= num_fea]
        self.true_crime = crime_true
        self.model_dates = crime_date
        self.days_of_past_crime = num_fea
        crime_dict = {'NormalizedDate': pd.Series(crime_date, index=crime_date)}
        for i in range(1, num_fea + 1):
            col_name = str(i) + "DaysAgo"
            crime_dict[col_name] = pd.Series(list(self.detrended)[num_fea - i: -i], index=crime_date)
        self.crime_features = pd.DataFrame(crime_dict, index=crime_date)
        self.normalize_crime()

    def normalize_crime(self):
        """Normalize the crime columns with the standard deviation, as detrended mean should already be ~zero"""
        fea_std = self.crime_features.loc[:, self.crime_features.columns != 'NormalizedDate'].values.std()
        for c in self.crime_features.columns:
            if c != 'NormalizedDate':
                self.crime_features[c] = self.crime_features[c] / fea_std

    def split_data(self, test_size):
        # Train Data
        self.X_train = self.crime_features.iloc[0:-test_size, self.crime_features.columns != 'NormalizedDate']
        self.Y_train = self.true_crime[0:-test_size]
        self.train_date = self.model_dates[0:-test_size]
        # Test Data
        self.X_test = self.crime_features.iloc[-test_size:, self.crime_features.columns != 'NormalizedDate']
        self.Y_test = self.true_crime[-test_size:]
        self.test_date = self.model_dates[-test_size:]

    # Additional Features
    def holiday_feature(self, holiday_only=True, type_exclude=None, past_n_days=False):
        """Create features related to if the date is a holiday"""
        holiday_file = r'..\Holidays.csv'
        holiday_df = pd.read_csv(holiday_file, parse_dates=['HolidayDate'])
        if past_n_days:
            dates_to_check = set(holiday_df.loc[holiday_df.Type != type_exclude, 'HolidayDate'])
            self.crime_features['IsHoliday'] = self.crime_features.NormalizedDate.apply(lambda x: x in dates_to_check)
            self.crime_features['HolidayPastDays'] = [np.sum(self.crime_features.IsHoliday[max(0, i-past_n_days):i])
                                                    for i, x in enumerate(self.crime_features.IsHoliday)]
        elif holiday_only:
            dates_to_check = set(holiday_df.loc[holiday_df.Type != type_exclude, 'HolidayDate'])
            self.crime_features['IsHoliday'] = self.crime_features.NormalizedDate.apply(lambda x: x in dates_to_check)
        if not holiday_only:
            # Column for each Holiday
            self.crime_features = self.crime_features.merge(holiday_df, how='left',
                                                            left_on='NormalizedDate', right_on='HolidayDate')
            del self.crime_features['HolidayDate']
            del self.crime_features['Type']
            if past_n_days:
                del self.crime_features['IsHoliday']
            self.crime_features = pd.get_dummies(self.crime_features, columns=['Holiday'])

    def date_features(self):
        """Add features related to the date and normalize them"""
        self.crime_features['weekday'] = self.crime_features.NormalizedDate.dt.weekday / 6.0
        self.crime_features['weekNumber'] = self.crime_features.NormalizedDate.dt.week / 53.0

    def movie_feature(self):
        """Add Feature of the number of movies released in theaters that week"""
        movie_df = pickle.load(open('ReleasedMovies.pkl', 'rb'))
        movie_df['Year_Week'] = [(y, w) for y, w in zip(movie_df.ReleaseDate.dt.year, movie_df.ReleaseDate.dt.week)]
        movie_counts = movie_df.groupby(['Year_Week']).ReleasedMovie.count()
        grouped_movie = pd.DataFrame({'Year_Week': list(movie_counts.index), 'MovieCount': list(movie_counts)},
                                     index=range(len(movie_counts)))
        self.crime_features['Year_Week'] = [(y, w) for y, w in zip(self.crime_features.NormalizedDate.dt.year,
                                                                   self.crime_features.NormalizedDate.dt.week)]
        self.crime_features = self.crime_features.merge(grouped_movie, how='left', on='Year_Week').fillna(0)
        del self.crime_features['Year_Week']
        # Normalize the Count
        self.crime_features['MovieCount'] = self.crime_features['MovieCount'] / 7.0

    def weather_feature(self):
        weather_df = pd.read_csv(r'..\WeatherData.csv', parse_dates=['Date'])
        weather_df['Precipitation'] = weather_df.Precipitation.fillna(0)
        weather_df['NewSnow'] = weather_df.NewSnow.fillna(0)
        weather_df['Avg'] = self.min_max_scaler(weather_df.Avg, weather_df.Avg.min(), weather_df.Avg.max())
        weather_df['Precipitation'] = self.min_max_scaler(weather_df.Precipitation,
                                                          weather_df.Precipitation.min(), weather_df.Precipitation.max())
        weather_df['NewSnow'] = self.min_max_scaler(weather_df.NewSnow, weather_df.NewSnow.min(), weather_df.NewSnow.max())
        self.crime_features = self.crime_features.merge(weather_df.loc[:,['Date', 'Avg', 'NewSnow', 'Precipitation']],
                                                        how='left', left_on='NormalizedDate', right_on='Date')
        del self.crime_features['Date']

    def model_report(self, model_type=None):
        """Return values related to the best models in each model"""
        model_names = []
        model_mse = []
        model_pred = []
        model_features = []
        for m in self.district_models:
            if model_type is not None and m.Type not in model_type:
                pass
            model_names.append(m.name)
            model_mse.append(m.best_mse)
            if m.best_feature is not None:
                model_pred.append(m.features[m.best_feature]['test_prediction'])
                fea_imp = [(k, v) for k, v in zip(m.features[m.best_feature]['features'],
                                                  m.features[m.best_feature]['feature_importance'])]
                model_features.append(fea_imp)
            else:
                model_pred.append(m.features['test_prediction'])
                model_features.append([('feature', m.features['feature_importance'])])
        return model_names, model_mse, model_pred, model_features

    def keep_top_models(self, num_models):
        """Based on the MSE score - keep only the top n models"""
        model_names, model_mse, model_pred, model_features = self.model_report(None)
        model_rank = sorted([(n, m, f, p) for n, m, f, p in zip(model_names, model_mse, model_features, model_pred)],
                            key=lambda x: x[1])
        mse_filter = model_rank[num_models - 1][1]
        model_copy = self.district_models.copy()
        for m in model_copy:
            if m.best_mse > mse_filter:
                self.district_models.remove(m)

    # Plots
    def plot_day_average(self):
        """Plot the day average for a district"""
        plt.figure(figsize=(18, 10))
        plt.plot(self.dates[self.day_average_days:], self.day_average, label=self.name)
        plt.title("{} Day Average in Crime Across {}".format(self.day_average_days, self.name))
        plt.show()

    def plot_detrended(self, show_anomalies=True):
        """Plot the detrended data with anomalies if desired"""
        plt.figure(figsize=(18, 10))
        plt.plot(self.dates[self.day_average_days:], self.detrended, label=self.name)
        if show_anomalies:
            anom_dates = [d for i, d in enumerate(self.dates[self.day_average_days:]) if i in self.anomaly_index]
            anom_vals = [d for i, d in enumerate(self.detrended) if i in self.anomaly_index]
            plt.scatter(anom_dates, anom_vals, c='red')
        plt.legend()
        plt.show()


class DistrictModel:
    def __init__(self, name, model, model_type, X_train, Y_train, X_test, Y_test, max_days):
        self.model = model
        self.name = name
        self.model_type = model_type
        self.features = collections.defaultdict(dict)
        # Model parameters
        self.X_train = X_train
        self.Y_train = Y_train
        self.X_test = X_test
        self.Y_test = Y_test
        self.max_crime_days = max_days
        # Note for Best model
        self.best_mse = None
        self.best_feature = None

    def svr_model(self, crime_days, X_train, X_test):
        """Features of a LinearSVR model"""
        self.model.fit(X_train, self.Y_train)
        test_prediction = self.model.predict(X_test)
        test_prediction = [round(x, 0) for x in test_prediction]
        self.features[crime_days]['test_prediction'] = test_prediction
        self.features[crime_days]['feature_importance'] = self.model.coef_
        self.features[crime_days]['features'] = X_train.columns
        self.features[crime_days]['mse'] = mean_squared_error(self.Y_test, test_prediction)

    def tree_model(self, crime_days, X_train, X_test):
        """Features of a tree ensemble model"""
        self.model.fit(X_train, [int(x * 1000) for x in self.Y_train])
        test_prediction = self.model.predict(X_test)
        test_prediction = [round(x / 1000, 0) for x in test_prediction]
        self.features[crime_days]['test_prediction'] = test_prediction
        self.features[crime_days]['feature_importance'] = self.model.feature_importances_
        self.features[crime_days]['features'] = X_train.columns
        self.features[crime_days]['mse'] = mean_squared_error(self.Y_test, test_prediction)

    def linear_model(self, crime_days, X_train, X_test):
        """Generate a linear Model"""
        self.model.fit(X_train, self.Y_train)
        test_prediction = self.model.predict(X_test)
        test_prediction = [round(x, 0) for x in test_prediction]
        self.features[crime_days]['y_intercept'] = self.model.intercept_
        self.features[crime_days]['test_prediction'] = test_prediction
        self.features[crime_days]['feature_importance'] = self.model.coef_
        self.features[crime_days]['features'] = X_train.columns
        self.features[crime_days]['mse'] = mean_squared_error(self.Y_test, test_prediction)

    def generate_linear_models(self):
        """Generate a model for each combination of crime days"""
        X_train = self.X_train.copy()
        X_test = self.X_test.copy()
        # Work backwards from max_crime_days
        for i in range(self.max_crime_days, 1, -1):
            if self.model_type == 'Linear':
                self.linear_model(i, X_train, X_test)
            elif self.model_type == 'Tree':
                self.tree_model(i, X_train, X_test)
            elif self.model_type == 'SVR':
                self.svr_model(i, X_train, X_test)
            col_name = str(i) + "DaysAgo"
            del X_train[col_name]
            del X_test[col_name]
        self.best_model()

    def simplest_model(self):
        train_average = round(np.mean(self.Y_train), 0)
        test_prediction = [train_average for x in self.X_test.iloc[:, 0]]
        test_prediction = [round(x, 0) for x in test_prediction]
        self.features['test_prediction'] = test_prediction
        self.features['feature_importance'] = train_average
        model_mse = mean_squared_error(self.Y_test, test_prediction)
        self.features['mse'] = model_mse
        self.best_mse = model_mse

    def basic_average(self):
        for i in range(self.max_crime_days, 0, -1):
            test_prediction = np.mean(self.X_test.iloc[:, 0:i], axis=1)
            test_prediction = [round(x, 0) for x in test_prediction]
            self.features[i]['test_prediction'] = test_prediction
            self.features[i]['feature_importance'] = [1] * i
            self.features[i]['features'] = self.X_test.columns[0:i]
            self.features[i]['mse'] = mean_squared_error(self.Y_test, test_prediction)
        self.best_model()

    def best_model(self):
        for k, v in self.features.items():
            if self.best_mse is None or v['mse'] < self.best_mse:
                self.best_mse = v['mse']
                self.best_feature = k

    def clear_model(self):
        """Free some memory"""
        self.model = None


if __name__ == '__main__':
    pass
