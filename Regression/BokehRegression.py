"""File for generating Bokeh related to the crime numbers"""

import pickle
import os
import pandas as pd
import numpy as np
from datetime import datetime

from sklearn.metrics import mean_squared_error
from bokeh.plotting import figure
from bokeh.io import curdoc
from bokeh.models import HoverTool, LinearColorMapper
from bokeh.models import ColumnDataSource, LabelSet
from bokeh.models.widgets import Div, Select, DateRangeSlider, RadioButtonGroup
from bokeh.layouts import widgetbox, column, row, layout
from bokeh.models.widgets import DataTable, DateFormatter, TableColumn, NumberFormatter

WORKING_DIR = os.path.dirname(os.path.abspath(__file__))
PICKLE_DF = 'BokehData.pkl'
PICKLE_MAP = 'ChicagoDistricts.pkl'
END = pd.Timestamp("2019-02-08")

## FUNCTIONS
def summary_text(model, baseline_mse, model_mse):
    the_text = """
                <b style="color:green">Model:</b> {}        <b style="color:green">Baseline MSE:</b> {:,.2f}         <b style="color:green">Model MSE:</b> {:,.2f}  
                """.format(model, baseline_mse, model_mse)
    return the_text


def date_text(start_date, end_date):
    the_text = """Showing Data from {} to {}""".format(start_date, end_date)
    return the_text


def map_crime_totals(my_df):
    total_actual_crime = my_df.groupby(['DistrictNum']).CrimeActual.sum().reset_index().fillna(0)
    total_predicted_crime = my_df.groupby(['DistrictNum']).CrimePred.sum().reset_index().fillna(0)
    # Create dictionary mapping district to crime values and return it
    crime_values = {d: [a, p] for d, a, p in
                    zip(total_actual_crime.DistrictNum, total_actual_crime.CrimeActual, total_predicted_crime.CrimePred)}
    return crime_values


def get_features(features, district):
    for x in features:
        if x[0] == district:
            sort_features = sorted(x[1], key=lambda x: np.abs(x[1]), reverse=False)
            feature_data = pd.DataFrame({'feature': [c[0] for c in sort_features],
                                         'feature_value': [abs(c[1]) for c in sort_features],
                                         'true_value': [c[1] for c in sort_features],
                                         'feature_x': [abs(c[1])*0.8 for c in sort_features],
                                         'feature_y': [i - 0.5 for i in range(len(x[1]))],
                                         'color': ['red' if c[1] < 0 else 'green' for c in sort_features],
                                         'feature_rank': range(len(x[1]))})
            return feature_data, x[2]


def min_max_scaler(data_to_scale):
    lower = data_to_scale.min()
    upper = data_to_scale.max()
    scaled = [(i - lower)/(upper - lower) if i is not None else None for i in data_to_scale]
    return scaled


def updating_map_source(df, map_chicago, total=True):
    crime_totals = map_crime_totals(df)
    # Crime totals is a dictionary that we will use to add columns to map_chicago
    map_chicago['ActualCrime'] = map_chicago.DIST_NUM.apply(
        lambda x: crime_totals[x][0] if crime_totals.get(x, None) is not None else 0)
    map_chicago['PredCrime'] = map_chicago.DIST_NUM.apply(
        lambda x: crime_totals[x][1] if crime_totals.get(x, None) is not None else 0)
    # Population per Crime
    map_chicago['ActualCrimePer1000'] = map_chicago.ActualCrime / map_chicago.Population * 1000
    map_chicago['PredCrimePer1000'] = map_chicago.PredCrime / map_chicago.Population * 1000
    # Min Max Scaler should disregard NA
    map_chicago['ActualNorm'] = min_max_scaler(map_chicago.ActualCrime)
    map_chicago['PredNorm'] = min_max_scaler(map_chicago.PredCrime)
    map_chicago['ActualRatioNorm'] = min_max_scaler(map_chicago.ActualCrimePer1000)
    map_chicago['PredRatioNorm'] = min_max_scaler(map_chicago.PredCrimePer1000)
    if total:
        map_chicago['ActualToPlot'] = map_chicago.ActualNorm
        map_chicago['PredToPlot'] = map_chicago.PredNorm
    else:
        map_chicago['ActualToPlot'] = map_chicago.ActualRatioNorm
        map_chicago['PredToPlot'] = map_chicago.PredRatioNorm
    return map_chicago


def save_pickle(result, pickle_name):
    """Saves an object in pickle object file for future use"""
    with open(os.path.join(WORKING_DIR, pickle_name), 'wb') as file:
        pickle.dump(result, file)
        return True


def load_pickle(pickle_name):
    """Loads a previously stored model dictionary with pickle"""
    with open(os.path.join(WORKING_DIR, pickle_name), 'rb') as handle:
        return pickle.load(handle)


### PICKLE FACTORY ###

if os.path.exists(os.path.join(WORKING_DIR, PICKLE_DF)):
    df, features = load_pickle(PICKLE_DF)
else:
    print('Missing df pickle : run ChicagoRegressionMain.py !')

if os.path.exists(os.path.join(WORKING_DIR, PICKLE_DF)):
    map_chicago = load_pickle(PICKLE_MAP)
else:
    print('Missing District Pickle!')

print(features[0])
# Hot Spots - top places of crime
hotspots = load_pickle(r'HotSpots.pkl')
hotspots_df = pd.DataFrame({'x': [r[0][1] for r in hotspots],
                            'y': [r[0][0] for r in hotspots],
                            'Crimes': [r[1] for r in hotspots]})
# Public Transport
#metro = load_pickle(r'ChicagoMetro.pkl')
#metro['x'] = metro.x.astype(float)
#metro['y'] = metro.y.astype(float)
#print(metro.columns, metro.head(), metro.info())

# ['Baseline', 'CrimeActual', 'CrimePred', 'District', 'NormalizedDate']
print(df.columns, map_chicago.columns)

## DATA SOURCE ##
map_chicago = map_chicago.loc[map_chicago.DIST_NUM != 31, :].sort_values(['DIST_NUM'])

full_source = ColumnDataSource(df)
district_source = ColumnDataSource(df.loc[(df.DistrictNum == 1) & (df.NormalizedDate <= END), :])
feature_df, model_name = get_features(features, "District1")
feature_source = ColumnDataSource(feature_df)
hotspot_source = ColumnDataSource(hotspots_df)
#metro_source = ColumnDataSource(metro)
map_chicago = updating_map_source(df.loc[df.NormalizedDate <= END, :], map_chicago)
map_source = ColumnDataSource(map_chicago)

print(map_chicago.columns)
## SLIDERS ##
date_slider = DateRangeSlider(start=min(df['NormalizedDate']),
                              end=max(df['NormalizedDate']),
                              step=1000 * 60 * 60 * 24,
                              value=(pd.Timestamp(min(df['NormalizedDate'])), pd.Timestamp("2019-02-08")),
                              title='Date Range',
                              width=400)

district_choices = list(df.DistrictNum.unique())
district_labels = list(df.District.unique())
district_options = sorted([(c, l) for c, l in zip(district_choices, district_labels)], key=lambda x: x[0])
district_select = Select(title="District:", value=district_options[0][1], options=district_options)

# Population vs Total Toggle
ActualRatioToggle = RadioButtonGroup(labels=["Total Crime", "Population Ratio"], active=0)

## TEXT ##

title = Div(width=800)
title.text = "<h1>Chicago Crime Regression</h1>"

date_summary = Div(width=700)
date_summary.text = date_text(datetime.date(min(df.NormalizedDate)),
                              datetime.date(END))

# Model Summary Text
model_summary = Div(width=700)
# MSE Calculations
baseline_mse = mean_squared_error(df.loc[(df.DistrictNum == 1), 'CrimeActual'][~df.CrimeActual.isna()],
                                  df.loc[(df.DistrictNum == 1), 'Baseline'][~df.CrimeActual.isna()])
model_mse = mean_squared_error(df.loc[(df.DistrictNum == 1), 'CrimeActual'][~df.CrimeActual.isna()],
                               df.loc[(df.DistrictNum == 1), 'CrimePred'][~df.CrimeActual.isna()])
model_summary.text = summary_text(model_name, baseline_mse, model_mse)

## PLOTS ##
color_mapper = LinearColorMapper(palette=['#800026', '#bd0026', '#e31a1c', '#fc4e2a', '#fd8d3c',
                                          '#feb24c', '#fed976', '#ffeda0', '#ffffcc'][::-1], low=0, high=1)
true_grid = figure(title="Actual Distribution of Crime by District", x_range=(-88.0, -87.5), y_range=(41.64, 42.04))
true_grid.patches('x', 'y', source=map_source,
                  fill_color={'field': 'ActualToPlot', 'transform': color_mapper},
                  fill_alpha=0.8, line_color="black", line_width=0.5)
true_grid.circle('x', 'y', source=hotspot_source, color='black', legend="HotSpot", size=2)
labels = LabelSet(x='Xmid', y='Ymid', text='DIST_NUM', source=map_source) #, render_mode='canvas', level='glyph')
true_grid.add_layout(labels)
true_grid.add_tools(HoverTool(tooltips=[
    ("District", "@DIST_NUM{0,0}"),
    ("Actual", "@ActualCrime{0,0}"),
    ("Prediction", "@PredCrime{0,0}"),
    ("Population", "@Population{0,0}"),
    ("Actual Crime per 1000", "@ActualCrimePer1000{0,0}"),
    ("Predicted Crime per 1000", "@PredCrimePer1000{0,0}")]))


pred_grid = figure(title="Predicted Distribution of Crime by District", x_range=(-88.0, -87.5), y_range=(41.64, 42.04))
pred_grid.patches('x', 'y', source=map_source,
                  fill_color={'field': 'PredToPlot', 'transform': color_mapper},
                  fill_alpha=0.8, line_color="black", line_width=0.5)
pred_grid.circle('x', 'y', source=hotspot_source, color='black', legend="HotSpot", size=2)
labels = LabelSet(x='Xmid', y='Ymid', text='DIST_NUM', source=map_source) #, render_mode='canvas', level='glyph')
pred_grid.add_layout(labels)
pred_grid.add_tools(HoverTool(tooltips=[
    ("District", "@DIST_NUM{0,0}"),
    ("Actual", "@ActualCrime{0,0}"),
    ("Prediction", "@PredCrime{0,0}"),
    ("Population", "@Population{0,0}"),
    ("Actual Crime per 1000", "@ActualCrimePer1000{0,0}"),
    ("Predicted Crime per 1000", "@PredCrimePer1000{0,0}")]))


# district prediction vs actual
pred_actual = figure(title="Crime Volume by Day", width=1200, height=500, x_axis_type="datetime")
pred_actual.line('NormalizedDate', 'CrimeActual', source=district_source, line_width=1, line_color='gray',
                 legend="Actual")
pred_actual.line('NormalizedDate', 'Baseline', source=district_source, line_width=1, line_color='blue',
                 line_dash="dotdash", legend="Base Model")
pred_actual.line('NormalizedDate', 'CrimePred', source=district_source, line_width=2, line_color='green',
                 line_dash="dashed", legend="Prediction")
pred_actual.xaxis.axis_label = "Date"
pred_actual.yaxis.axis_label = "Crime Volume"
pred_actual.legend.location = "bottom_right"
pred_actual.add_tools(HoverTool(tooltips=[
    ("Date", "@NormalizedDate{%d %b %Y}"),
    ("Actual", "@CrimeActual"),
    ("Prediction", "@CrimePred")], formatters={'NormalizedDate': 'datetime'}))

# Grid Table
grid_table_columns = [
    TableColumn(field="DIST_NUM", title="District", formatter=NumberFormatter(format='0,0')),
    TableColumn(field="ActualCrime", title="Actual Crime Volume", formatter=NumberFormatter(format='0,0')),
    TableColumn(field="PredCrime", title="Predicted Crime Volume", formatter=NumberFormatter(format='0,0')),
    TableColumn(field="Population", title="Population", formatter=NumberFormatter(format='0,0')),
    TableColumn(field="ActualCrimePer1000", title="Actual Crimes:1000", formatter=NumberFormatter(format='0,0')),
    TableColumn(field="PredCrimePer1000", title="Predicted Crimes:1000", formatter=NumberFormatter(format='0,0'))
]
grid_data_table = DataTable(source=map_source, columns=grid_table_columns, height=600)  # , width=650, height=800)


# Feature Plot - stored [district, [(feature: value)]
the_model_features = list(feature_df.feature)
importance_plot = figure(width=1200, height=500, title="Model Feature Importance")
importance_plot.hbar(y='feature_rank', right='feature_value', height=0.9, source=feature_source, color='color', alpha=0.5)
# Set some properties to make the plot look better
importance_plot.ygrid.grid_line_color = None
importance_plot.xaxis.axis_label = "Feature Importance"
importance_plot.yaxis.axis_label = "Feature Count"
importance_plot.add_tools(HoverTool(tooltips=[("Feature", "@feature"), ("Importance", "@true_value")]))
labels = LabelSet(x='feature_x', y='feature_y', text='feature', source=feature_source, text_font_size='10pt') #, render_mode='canvas', level='glyph')
importance_plot.add_layout(labels)

## UPDATES ##

def update(attr, old, new):
    """Update text and data sources when date filter changes"""
    d = date_slider.value
    # convert to date, as issues with getting the first date back
    start_date = datetime.date(min(df.NormalizedDate))
    end_date = END
    print(type(d[0]))
    if type(d[0]) in [int]:
        start_date = datetime.date(datetime.fromtimestamp(d[0] / 1000))
        end_date = datetime.date(datetime.fromtimestamp(d[1] / 1000))
    # update text and data sources
    date_summary.text = date_text(start_date, end_date)
    district_val = district_select.value
    if district_val == 'District1':
        district_val = 1
    # Update all Data Sources
    df_ = df.loc[
          (df.DistrictNum == int(district_val)) & (df.NormalizedDate >= start_date) & (df.NormalizedDate <= end_date),
          :]
    district_source.data = district_source.from_df(df_)
    new_features_df, model_name = get_features(features, "District" + str(district_val))
    feature_source.data = feature_source.from_df(new_features_df)
    # Get Toggle Value to Pass
    toggle = False
    if ActualRatioToggle.active == 0:
        toggle = True
    map_chicago_new = updating_map_source(df.loc[(df.NormalizedDate >= start_date) & (df.NormalizedDate <= end_date),:], map_chicago, toggle)
    map_source.data = map_source.from_df(map_chicago_new)
    baseline_mse = mean_squared_error(df_.CrimeActual[~df_.CrimeActual.isna()],
                                      df_.Baseline[~df_.CrimeActual.isna()])
    model_mse = mean_squared_error(df_.CrimeActual[~df_.CrimeActual.isna()],
                                   df_.CrimePred[~df_.CrimeActual.isna()])
    model_summary.text = summary_text(model_name, baseline_mse, model_mse)


date_slider.on_change('value', update)
district_select.on_change('value', update)
ActualRatioToggle.on_change('active', update)


## LAYOUT
doc_layout = layout(sizing_mode='scale_both')
doc_layout.children.append(row(title))
doc_layout.children.append(row(widgetbox(ActualRatioToggle, width=300), widgetbox(date_summary)))
doc_layout.children.append(row(true_grid, pred_grid))
# doc_layout.children.append(row(grid_data_table))
doc_layout.children.append(row(widgetbox(district_select, width=400), date_slider))
doc_layout.children.append(row(widgetbox(model_summary)))
doc_layout.children.append(row(pred_actual))
doc_layout.children.append(row(importance_plot))
curdoc().add_root(doc_layout)

# major_label_overrides¶
