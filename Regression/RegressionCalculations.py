"""Use the calculated best Models to get predictions for the test data"""

import ChicagoClasses
import pandas as pd
import numpy as np
import re
import pickle
import os
import datetime


from sklearn.metrics import mean_squared_error
from sklearn.linear_model import LinearRegression
from sklearn.svm import LinearSVR
import sqlite3


BEST_FILE = r'BestModels.pkl'
#SQL_FILE = r'..\..\CnvrgData\ChicagoCrime.sqlite'
SQL_FILE = r'..\RefreshChicagoCrime.sqlite'
DATE_COLS = ['Date', 'UpdatedOn', 'NormalizedDate']


def load_data(sql_file=SQL_FILE, date_cols=DATE_COLS):
    with sqlite3.connect(sql_file) as con:
        df = pd.read_sql_query(
            """ SELECT * from chicagoCrime where CAST(strftime('%Y', NormalizedDate) as INTEGER) >= 2015""",
            con, parse_dates=date_cols)
    return df


def format_dataframe(df):
    """Format the columns in the dataframe"""
    df['Arrest'] = df.Arrest.apply(lambda x: 1 if x == 'true' else 0)
    df['Domestic'] = df.Domestic.apply(lambda x: 1 if x == 'true' else 0)
    df['Latitude'] = df.Latitude.apply(lambda x: float(x) if x != '' else None)
    df['Longitude'] = df.Longitude.apply(lambda x: float(x) if x != '' else None)
    df['District'] = df.District.apply(lambda x: int(x) if x != '' else None)
    df['Ward'] = df.Ward.apply(lambda x: int(x) if x != '' else None)
    df['CommunityArea'] = df.Ward.apply(lambda x: x if x != '' else None)
    df['XCoordinate'] = df.XCoordinate.apply(lambda x: x if x != '' else None)
    df['YCoordinate'] = df.YCoordinate.apply(lambda x: x if x != '' else None)
    # get rid of NA and district 31
    filtered_df = df.dropna()
    filtered_df = filtered_df.loc[filtered_df.District != 31, :]
    return filtered_df


def save_df(df, pickle_file):
    """Save a pickle object with a given name"""
    pickle.dump(df, open(pickle_file, 'wb'))


def retrieve_data(dataframe_file, multi=False):
    """Runs the main project"""
    file_directory = os.path.dirname(os.path.abspath(__file__))
    model_file = os.path.join(file_directory, dataframe_file)
    if os.path.exists(model_file):
            return pickle.load(open(model_file, 'rb'))
    print("Run Chicago Regression!")
    return


def create_best(features, district):
    """Recreate model with best features"""
    test_days = 90
    if features['type'] == 'Linear':
        model = LinearRegression()
        print('Linear')
    else:
        model = LinearSVR(random_state=42, max_iter=1000, fit_intercept=False,
                          loss='squared_epsilon_insensitive', dual=False)
        print('SVR')
    # Detrend with 30 days
    district.n_day_average(30)
    district.generate_features(features['CrimeDays'])
    # Check for Weather & Movie
    model_name = features['model']
    if 'Movie' in model_name:
        district.movie_feature()
        print('Movie')
    if 'Weather' in model_name:
        district.weather_feature()
        print('Weather')
    if 'Dates' in model_name:
        district.date_features()
        print('Dates')
    if 'Holiday' in model_name:
        past_days=False
        holiday_only = True
        if 'Past' in model_name:
            past_days = 4
        if 'Each' in model_name:
            holiday_only = False
        district.holiday_feature(holiday_only, None, past_days)
        print('Holiday: ', past_days, holiday_only)
    # Data has been updated with correct features
    district.split_data(test_days)
    model.fit(district.X_train, district.Y_train)
    test_pred = model.predict(district.X_test)
    district_info = {'NormalizedDate': district.test_date,
                     'District': [district.name] * len(district.test_date),
                     'CrimeActual': district.daily_crime[-test_days:],
                     'Baseline': simplest_model(district)}
    district_info['CrimePred'] = [round(p + t, 0) for p, t in
                                  zip(test_pred, district.day_average[-test_days:])]
    district_features = sorted([(k, v) for k, v in zip(district.X_train.columns, model.coef_)],
                               key=lambda x: abs(x[1]), reverse=True)
    return district_info, district_features, model


def simplest_model(district):
    test_pred = district.day_average[-90:]
    # mse = mean_squared_error(district.Y_test, test_pred)
    return test_pred


def predict_future(district, model, features, data, days=20):
    """Predict out into the future"""
    # Set up DFs to reference
    new_df = district.X_test.iloc[-4:, :].copy()
    last_x = district.X_test.iloc[-1, :].copy()
    last_y = district.Y_test[-1]
    last_d = district.test_date[-1]
    last_30y = list(district.daily_crime[-30:])
    crime_avg = np.min(district.daily_crime[:-90])
    crime_std = np.max(district.daily_crime[:-90])
    last_y = (last_y - crime_avg) / (crime_std)
    day_avg = []
    day_avg.append(np.mean(last_30y))
    new_dates = []
    preds = []
    new_dates.append(district.test_date[-1] + datetime.timedelta(days=1))
    for i in range(days):
        new_line = get_new_line(last_x, last_y, features, new_dates, new_df.columns, data)
        new_df = pd.concat([new_df, new_line], axis=0, ignore_index=True, join='inner')
        last_y = model.predict(new_df.iloc[-1,:].reshape(1, -1))[0]
        last_x = new_df.iloc[-1,:].copy()
        last_30y.append(last_y + day_avg[-1])
        last_y = (last_30y[-1] - crime_avg)/crime_std
        preds.append(last_y)
        day_avg.append(np.mean(last_30y[-30:]))
        new_dates.append(new_dates[-1] + datetime.timedelta(days=1))
    # have predictions, have dates, construct df to return
    district_info = {'NormalizedDate': new_dates[:-1],
                     'District': [district.name] * (len(new_dates)-1)}
    # Do we try and recalculate an average?
    district_info['CrimePred'] = [round(p + t, 0) for p, t in
                                  zip(preds, day_avg)]
    return pd.DataFrame(district_info)


def get_new_line(last_line, new_value, features, prior_date, columns, data):
    """Generate a new line"""
    crimeDays = features['CrimeDays']
    crime_info = {}
    for i in range(1, crimeDays+1):
        label = str(i) + 'DaysAgo'
        copy_label = str(i-1) + 'DaysAgo'
        if i == 1:
            crime_info[label] = new_value
        else:
            crime_info[label] = last_line[copy_label]
    # Now Need to add Movie, Weather, Date, Holiday
    crime_info['NormalizedDate'] = prior_date[-1]
    for c in columns:
        if 'DaysAgo' in c:
            pass
        else:
            crime_info[c] = data.loc[data.Date == crime_info['NormalizedDate'], c]
    del crime_info['NormalizedDate']
    return pd.DataFrame(crime_info)


def main():
    city_data = []
    city_features = []
    df, best_models = retrieve_data(BEST_FILE)
    if best_models is None:
        return
    df = retrieve_data('RefreshDataframe.sav')
    if df is None:
        df = load_data(SQL_FILE, DATE_COLS)
        df = format_dataframe(df)
        save_df(df, 'RefreshDataframe.sav')
    february_pred = pd.read_csv(r'February2019Features.csv', parse_dates=['Date'])
    # Have data in DF, can get appropriate model with the features and train on whole timeperiod
    city_crime = ChicagoClasses.ChicagoCrime(df)
    city_crime.create_districts()
    dict_features = {r[0]: {'model': r[2],
                            'type': r[3],
                            'CrimeDays': r[4]} for r in best_models}
    for d in city_crime.districts:
        model_features = dict_features[d.name]
        d_info, d_features, model = create_best(model_features, d)
        city_data.append(pd.DataFrame(d_info))
        # line here for the future prediction and append to city_data
        d_forecast = predict_future(d, model, model_features, february_pred)
        city_data.append(d_forecast)
        city_features.append([d.name, d_features, model_features['model'], model_features['type'], model_features['CrimeDays'], model])
    city_predictions = pd.concat(city_data, ignore_index=True, axis=0)
    city_predictions['DistrictNum'] = city_predictions.District.apply(lambda x: int(re.search(r'[\d]+', x).group()))
    save_df([city_predictions, city_features], "BokehData.pkl")


if __name__ == '__main__':
    main()