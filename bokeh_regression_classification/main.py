import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn import manifold

import os
import pickle
import sys
import sqlite3
import math
import datetime

from bokeh.palettes import Category20 as palette
import itertools
import bokeh.models as bmo
from bokeh.palettes import d3

from sklearn.metrics import confusion_matrix
import numpy as np
from bokeh.models.widgets import CheckboxGroup, MultiSelect, Select, DateRangeSlider, RangeSlider, Button, RadioGroup, MultiSelect, Div, Slider, Dropdown, Paragraph, TableColumn, DateFormatter, DataTable, DateSlider, CheckboxButtonGroup, RadioButtonGroup
from bokeh.io import curdoc
from bokeh.layouts import widgetbox
from bokeh.layouts import row, column
from bokeh.plotting import figure
from bokeh.plotting import show, ColumnDataSource
from bokeh.models import ColumnDataSource, Label, HoverTool, Range1d
from bokeh.models import LabelSet
from bokeh.models.annotations import Title

from bokeh.tile_providers import CARTODBPOSITRON
from bokeh.tile_providers import STAMEN_TONER

### GLOBAL VARIABLES ###
WORKING_DIR = os.path.dirname(os.path.abspath(__file__))
PICKLE_DF_TRUE = 'future_df_true.pkl'
PICKLE_DF_PREV = 'future_df_prev.pkl'

### PICKLE FACTORY ###

def save_pickle(result, pickle_name):
    """Saves an object in pickle object file for future use"""
    with open(os.path.join(WORKING_DIR, pickle_name), 'wb') as file:
        pickle.dump(result, file)
        return True

def load_pickle(pickle_name):
    """Loads a previously stored model dictionary with pickle"""
    with open(os.path.join(WORKING_DIR, pickle_name), 'rb') as handle:
        return pickle.load(handle)


if os.path.exists(os.path.join(WORKING_DIR, PICKLE_DF_TRUE)):
    df_true = load_pickle(PICKLE_DF_TRUE)
else:
    print('Missing df pickle : run bokeh_modeling.py !')

if os.path.exists(os.path.join(WORKING_DIR, PICKLE_DF_PREV)):
    df_prev = load_pickle(PICKLE_DF_PREV)
else:
    print('Missing feat_importance pickle : run bokeh_modeling.py !')


def merc(Coords):
    lat = Coords[0]
    lon = Coords[1]

    r_major = 6378137.000
    x = r_major * math.radians(lon)
    scale = x / lon
    y = 180.0 / math.pi * math.log(math.tan(math.pi / 4.0 +
                                            lat * (math.pi / 180.0) / 2.0)) * scale
    return (x, y)


df_true['coords_x'] = df_true.apply(lambda x: merc((x.Latitude, x.Longitude))[0], axis=1)
df_true['coords_y'] = df_true.apply(lambda x: merc((x.Latitude, x.Longitude))[1], axis=1)

df_prev['coords_x'] = df_prev.apply(lambda x: merc((x.Latitude, x.Longitude))[0], axis=1)
df_prev['coords_y'] = df_prev.apply(lambda x: merc((x.Latitude, x.Longitude))[1], axis=1)


## DATA SOURCE ##

source_true = ColumnDataSource(df_true)
source_predict = ColumnDataSource(df_prev)


## MODELS ##

model_menu = [('Prediction_RF', "Random Forest"), ('Prediction_KNN', "K-Nearest Neighbors"), ('Prediction_XGB', "XG Boost")]
model_dropdown = Select(title="Model", options=model_menu, value='Prediction_RF')

## IMGS ##
logo_chicago = os.path.join(os.path.basename(os.path.dirname(__file__)), "static", "chicago.png")
logo_src_1 = ColumnDataSource(dict(url=[logo_chicago]))
image1 = figure(plot_width=100, plot_height=100, title="")
image1.toolbar.logo = None
image1.toolbar_location = None
image1.x_range = Range1d(start=0, end=1)
image1.y_range = Range1d(start=0, end=1)
image1.xaxis.visible = None
image1.yaxis.visible = None
image1.xgrid.grid_line_color = None
image1.ygrid.grid_line_color = None
image1.image_url(url='url', x=0, y=1, h=1, w=1, source=logo_src_1)
image1.outline_line_alpha = 0

logo_itc = os.path.join(os.path.basename(os.path.dirname(__file__)), "static", "itc.png")
logo_src_2 = ColumnDataSource(dict(url=[logo_itc]))
image2 = figure(plot_width=100, plot_height=100, title="")
image2.toolbar.logo = None
image2.toolbar_location = None
image2.x_range=Range1d(start=0, end=1)
image2.y_range=Range1d(start=0, end=1)
image2.xaxis.visible = None
image2.yaxis.visible = None
image2.xgrid.grid_line_color = None
image2.ygrid.grid_line_color = None
image2.image_url(url='url', x=0, y=1, h=1, w=1, source=logo_src_2)
image2.outline_line_alpha = 0

logo_city = os.path.join(os.path.basename(os.path.dirname(__file__)), "static", "batman3.png")
logo_src_3 = ColumnDataSource(dict(url=[logo_city]))
image3 = figure(plot_width=100, plot_height=100, title="")
image3.toolbar.logo = None
image3.toolbar_location = None
image3.x_range=Range1d(start=0, end=1)
image3.y_range=Range1d(start=0, end=1)
image3.xaxis.visible = None
image3.yaxis.visible = None
image3.xgrid.grid_line_color = None
image3.ygrid.grid_line_color = None
image3.image_url(url='url', x=0, y=1, h=1, w=1, source=logo_src_3)
image3.outline_line_alpha = 0

## SLIDERS ##

date_ = DateRangeSlider(title='Date',
                        value=(min(df_true['Date']), max(df_true['Date'])),
                        start=min(df_true['Date']),
                        end=max(df_true['Date']),
                        step=1)

hour_ = RangeSlider(title='Hour',
                        value=(min(df_true['Hour']), max(df_true['Hour'])),
                        start=min(df_true['Hour']),
                        end=max(df_true['Hour']),
                        step=1)

## DAY OF WEEK ##
day_of_week = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
#days = CheckboxButtonGroup(labels=day_of_week, active=list(range(len(day_of_week))))
days = MultiSelect(title='Day of Week',
                   size=7,
                   value=[str(i) for i in range(len(day_of_week))],
                   options=[(str(i), day) for i, day in enumerate(day_of_week)])

# SELECT DISTRICTS
district_list = [str(x) for x in sorted(pd.unique(df_true['District'].values))]
districts = MultiSelect(title='Districts',
                        size=5,
                        value=district_list,
                        options=[(str(district), district) for district in district_list])

## TEXT ##
main_title = Div(text="<b>Chicago crime FULL prediction : regression + classification</b>", style={'font-size': '150%'}, width=600, height=30)


categories = list(pd.unique(df_true['PrimaryType'].values))

type_of_crime = MultiSelect(title='Labels',
                            size=10,
                            value=['5'],
                            options=[(str(i), label) for i, label in enumerate(categories)])


p1 = figure(plot_width=580, plot_height=560, x_axis_type="mercator", y_axis_type="mercator", title=Title(text='Labels'), toolbar_location="below")
p1.title.text_font_size = '16pt'
p1.add_tile(STAMEN_TONER)
p1.axis.visible = False
p1.outline_line_width = 5
p1.outline_line_alpha = 0.3
p1.outline_line_color = "navy"

p2 = figure(plot_width=580, plot_height=560, x_axis_type="mercator", y_axis_type="mercator", title=Title(text='Predictions'), toolbar_location="below")
p2.title.text_font_size = '16pt'
p2.add_tile(STAMEN_TONER)
p2.axis.visible = False
p2.outline_line_width = 5
p2.outline_line_alpha = 0.3
p2.outline_line_color = "navy"

# create a color iterator
colors_no_green = palette[20][:4] + palette[20][6:]
color_map = bmo.CategoricalColorMapper(factors=pd.unique(df_true['PrimaryType'].values), palette=colors_no_green)

p1.scatter(source=source_true,
          x='coords_x',
          y='coords_y',
          #radius = 's',
          legend='PrimaryType',
          color={'field': 'PrimaryType', 'transform': color_map},
          marker="circle",
          alpha=0.75)


p2.scatter(source=source_predict,
          x='coords_x',
          y='coords_y',
          #radius = 's',
          legend='Prediction',
          color={'field': 'Prediction', 'transform': color_map},
          marker="circle",
          alpha=0.75)


p2.legend.click_policy="hide"
p1.legend.click_policy="hide"


## DEFINE CALLBACK UPDATE ##

def update_data(attrname, old, new):
    start_date_, end_date_ = date_.value_as_datetime
    start_hour_, end_hour_ = hour_.value
    category_index = [int(i) for i in type_of_crime.value]
    district_selected = [int(i) for i in districts.value]
    days_selected = [int(i) for i in days.value]
    prediction_col = model_dropdown.value

    df_prev_ = df_prev.copy()
    df_true_ = df_true.copy()

    df_true_ = df_true[df_true['Date'] >= start_date_][df_true['Date'] <= end_date_][df_true_['Hour'] >= start_hour_][df_true_['Hour'] <= end_hour_][df_true_['District'].isin(district_selected)][df_true_['Day'].isin(days_selected)]

    df_prev_ = df_prev_[df_prev_['Date'] >= start_date_][df_prev_['Date'] <= end_date_][df_prev_['Hour'] >= start_hour_][df_prev_['Hour'] <= end_hour_][df_prev_['District'].isin(district_selected)][df_prev_['Day'].isin(days_selected)]
    df_prev_['Prediction'] = df_prev_[prediction_col]

    source_true.data = source_true.from_df(
        df_true_[df_true_['PrimaryType'].isin([categories[i] for i in category_index])]
    )

    source_predict.data = source_predict.from_df(
        df_prev_[df_prev_['Prediction'].isin([categories[i] for i in category_index])]
    )

update_data(None, None, None)

date_.on_change('value', update_data)
hour_.on_change('value', update_data)
type_of_crime.on_change('value', update_data)
days.on_change('value', update_data)
districts.on_change('value', update_data)
model_dropdown.on_change('value', update_data)

curdoc().add_root(
    column(
        row(image1, image2, image3, main_title), row
        (
            column(model_dropdown, type_of_crime, date_, hour_, days, districts),
            column(p1),
            column(p2)
        )
    ))

curdoc().title = "Chicago map"